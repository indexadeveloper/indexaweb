﻿CREATE PROC ap_UserDomainGroup_Insert
@Groups_Id int=NULL
,@UserDomains_Id int=NULL
AS
INSERT INTO UserDomainGroup( 
[Groups_Id]
,[UserDomains_Id]
)
VALUES ( 
@Groups_Id
,@UserDomains_Id
)
SELECT SCOPE_IDENTITY() AS Id
