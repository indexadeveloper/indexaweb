namespace IndexaWeb.Intranet
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity.ModelConfiguration;
    using Models;
    public partial class IndexaWebModel : DbContext
    {
        public IndexaWebModel()
            : base("name=IndexaWebContext")
        {
            
        }

        public virtual DbSet<C__RefactorLog> C__RefactorLog { get; set; }
        public virtual DbSet<Activity> Activity { get; set; }
        //public virtual DbSet<AspNetUsers> User { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomersPerEmployee> CustomersPerEmployee { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EmployeeLevel> EmployeeLevel { get; set; }
        public virtual DbSet<Level> Level { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        //public virtual DbSet<Person> Person { get; set; }
        //public virtual DbSet<ProjectHeaders> ProjectHeaders { get; set; }
        public virtual DbSet<ProjectTask> ProjectTask { get; set; }
        public virtual DbSet<Reclaim> Reclaim { get; set; }
        public virtual DbSet<ReclaimComment> ReclaimComment { get; set; }
        public virtual DbSet<Representative> Representative { get; set; }
        public virtual DbSet<StatusTask> StatusTask { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<VisitDetail> VisitDetail { get; set; }
        public virtual DbSet<VisitHeader> VisitHeader { get; set; }
        public virtual DbSet<StatusVisit> StatusVisit { get; set; }
        public virtual DbSet<StatusVisitHistory> StatusVisitHistory { get; set; }
        //public virtual DbSet<vw_Employee> vw_Employee { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<IdentityUserRole>().HasKey((IdentityUserRole r) =>
                new { UserId = r.UserId, RoleId = r.RoleId }).ToTable("AspNetUserRoles");
            modelBuilder.Entity<IdentityUserLogin>().HasKey(u => new { u.UserId, u.LoginProvider, u.ProviderKey });

            modelBuilder.Entity<Customer>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersPerEmployee>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersPerEmployee>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);
            
            modelBuilder.Entity<Employee>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);
            
            modelBuilder.Entity<Location>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            //modelBuilder.Entity<Person>()
            //    .Property(e => e.CreatedBy)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Person>()
            //    .Property(e => e.ModifiedBy)
            //    .IsUnicode(false);
            

            modelBuilder.Entity<Reclaim>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<Reclaim>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Reclaim>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<ReclaimComment>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<ReclaimComment>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Representative>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Representative>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);
                            

            //modelBuilder.Entity<Representative>()
            //    .HasRequired(e => e.Customer)
            //    .WithMany(e => e.Representative)
            //    .WillCascadeOnDelete(false);


            modelBuilder.Entity<sysdiagrams>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<sysdiagrams>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<VisitDetail>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<VisitDetail>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<VisitHeader>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<VisitHeader>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);
            

            //modelBuilder.Entity<StatusVisit>()
            //    .HasMany(e => e.VisitHeader)
            //    .WithRequired(v => v.StatusVisit)
            //    .WillCascadeOnDelete(false);


            //modelBuilder.Entity<vw_Employee>()
            //    .Property(e => e.CreatedBy)
            //    .IsUnicode(false);

            //modelBuilder.Entity<vw_Employee>()
            //    .Property(e => e.ModifiedBy)
            //    .IsUnicode(false);

            //modelBuilder.Entity<vw_Employee>()
            //    .Property(e => e.EmployeeCreatedBy)
            //    .IsUnicode(false);

            //modelBuilder.Entity<vw_Employee>()
            //    .Property(e => e.EmployeeModifiedBy)
            //    .IsUnicode(false);
        }

        public System.Data.Entity.DbSet<IndexaWeb.Intranet.AspNetUsers> AspNetUsers { get; set; }

        public System.Data.Entity.DbSet<IndexaWeb.Intranet.Models.ProjectHeaders> ProjectHeader { get; set; }

        public System.Data.Entity.DbSet<IndexaWeb.Intranet.Models.StatusTaskHistories> StatusTaskHistories { get; set; }

        public System.Data.Entity.DbSet<IndexaWeb.Intranet.Models.SpendingHistory> SpendingHistories { get; set; }

        public System.Data.Entity.DbSet<IndexaWeb.Intranet.Models.PurchaseHeaders> PurchaseRMAs { get; set; }

        public System.Data.Entity.DbSet<IndexaWeb.Intranet.Models.Products> Products { get; set; }
    }
}
