﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;

namespace IndexaWeb.Intranet.Controllers
{
    public class ProjectTasksController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();
        // GET: ProjectTasks
        public async Task<ActionResult> Index(int? id, string name)
        {
            ViewBag.id = id;
            ViewBag.name = name;
            List<Names> names = new List<Names>();
            Names aux = new Names();

            foreach (var item in db.ProjectTask.Where(x => x.ProjectHeaderId == id).ToList())
            {
                aux.EstatusName = db.StatusTask.Where(x => x.StatusTaskID == item.StatusTaskId).First().Name;
                names.Add(aux);
                aux = new Names();
            }
            ViewBag.ListNames = names;
            ViewBag.indice = 0;
            return View(await db.ProjectTask.Where(x => x.ProjectHeaderId == id).ToListAsync());
        }

        // GET: ProjectTasks/Details/5
        public async Task<ActionResult> Details(int? id, int? idp, string name, string statusname)
        {
            ViewBag.idp = idp;
            ViewBag.name = name;
            ViewBag.id = id;
            ViewBag.statusname = statusname;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectTask projectTasks = await db.ProjectTask.FindAsync(id);
            if (projectTasks == null)
            {
                return HttpNotFound();
            }
            return View(projectTasks);
        }

        public async Task<ActionResult> DetailsAnulado(int? id, int? idp, string name, string statusname)
        {
            ViewBag.idp = idp;
            ViewBag.name = name;
            ViewBag.id = id;
            ViewBag.statusname = statusname;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectTask projectTasks = await db.ProjectTask.FindAsync(id);
            if (projectTasks == null)
            {
                return HttpNotFound();
            }
            return View(projectTasks);
        }

        // GET: ProjectTasks/Create
        public ActionResult Create(int? id, string name)
        {
            ViewBag.id = id;
            ViewBag.name = name;
            return View();
        }

        // POST: ProjectTasks/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProjectTaskID,ProjectHeaderId,StatusTaskID,TaskName,PublicationDate,StartingDate,EstimatedEnding,EndDate,TaskObservation")] ProjectTask projectTasks, int id, string name)
        {
            ViewBag.id = id;
            ViewBag.name = name;
            projectTasks.ProjectHeaderId = id;
            projectTasks.PublicationDate = DateTime.Now;
            projectTasks.StatusTaskId = 1;
            if (ModelState.IsValid)
            {
                db.ProjectTask.Add(projectTasks);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new { id = id, name = name });
            }

            return View(projectTasks);
        }

        // GET: ProjectTasks/Edit/5
        public async Task<ActionResult> Edit(int? id, int idp, string name)
        {
            ViewBag.id = id;
            ViewBag.idp = idp;
            ViewBag.name = name;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectTask projectTasks = await db.ProjectTask.FindAsync(id);
            
            if (projectTasks == null)
            {
                return HttpNotFound();
            }
            return View(projectTasks);
        }

        // POST: ProjectTasks/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AssignedToUser,ProjectTaskID,ProjectHeaderId,StatusTaskID,TaskName,PublicationDate,StartingDate,EstimatedEnding,EndDate,TaskObservation")] ProjectTask projectTasks, int id, int idp, string name)
        {
            try
            { 
                ViewBag.id = id;
                ViewBag.idp = idp;
                ViewBag.name = name;
                if(projectTasks.AssignedToUser != "" && projectTasks.AssignedToUser != null)
                {
                    projectTasks.StartingDate = DateTime.Now;
                    projectTasks.StatusTaskId = 2;
                }

                if (ModelState.IsValid)
                {
                    db.Entry(projectTasks).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = idp, name = name });
                }
                return View(projectTasks);
            }catch(Exception e)
            {
               return RedirectToAction("Edit", new { id = id,idp = idp, name = name });
            }
        }


        public async Task<ActionResult> Editar(int id, int idp, string name, ProjectTask task, int statusId)
        {
            ViewBag.id = id;
            ViewBag.idp = idp;
            ViewBag.name = name;
            db.Entry(task).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("ProjectTasks/Index", new { id = idp, name = name });

        }

        public async Task<ActionResult> EditFinalizado(int? id, int idp, string name)
        {
            ViewBag.id = id;
            ViewBag.idp = idp;
            ViewBag.name = name;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectTask projectTasks = await db.ProjectTask.FindAsync(id);
            if (projectTasks == null)
            {
                return HttpNotFound();
            }
            return View(projectTasks);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditFinalizado([Bind(Include = "ProjectTaskID,ProjectHeaderId,StatusTaskID,TaskName,PublicationDate,StartingDate,EstimatedEnding,EndDate,TaskObservation,AssignedToUser,Hours")] ProjectTask projectTasks, int id, int idp, string name)
        {
            ViewBag.id = id;
            ViewBag.idp = idp;
            ViewBag.name = name;
            if (ModelState.IsValid)
            {
                db.Entry(projectTasks).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new { id = idp, name = name });
            }
            return View(projectTasks);
        }


        // GET: ProjectTasks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectTask projectTasks = await db.ProjectTask.FindAsync(id);
            if (projectTasks == null)
            {
                return HttpNotFound();
            }
            return View(projectTasks);
        }

        // POST: ProjectTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProjectTask projectTasks = await db.ProjectTask.FindAsync(id);
            db.ProjectTask.Remove(projectTasks);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

