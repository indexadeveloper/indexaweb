﻿CREATE PROC ap_VisitDetail_Update
@VisitDetailId int=NULL
,@VisitHeaderid int=NULL
,@ActivityId int=NULL
,@VisitNotes nvarchar (500)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE VisitDetail 
SET 
[VisitHeaderid]=ISNULL(@VisitHeaderid,VisitHeaderid)
,[ActivityId]=ISNULL(@ActivityId,ActivityId)
,[VisitNotes]=ISNULL(@VisitNotes,VisitNotes)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [VisitDetailId]=@VisitDetailId
