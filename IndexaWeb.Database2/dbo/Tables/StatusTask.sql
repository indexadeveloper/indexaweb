﻿CREATE TABLE [dbo].[StatusTask] (
    [StatusTaskID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([StatusTaskID] ASC)
);

