﻿using IndexaWeb.WindowsForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Net.Http.Headers;
using System.IO;
using System.Threading;

namespace IndexaWeb.WindowsForm
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void _submitButton_Click(object sender, EventArgs e)
        {
            var username = _userNameTextBox.Text;
            var password = _passwordTextBox.Text;
            //var token = TokenResponse.GetBearerToken("http://localhost:65373/", username, password);
            //token.Wait();
            _nameLabel.Text = "Cargando...";
            //var resp = GetWebApiRequest("http://localhost:65373/", $"~/Api/GetUserByCredentials?username={username}&password={password}", username, password);
            //resp.GetAwaiter();
            using (var client = ClientHelper.GetClient(username, password))
            {
            }
                        
        }
        static internal async Task<string> GetWebApiRequest(string siteUrl, string apiController, string userName, string password)
        {
            TokenResponseModel Token = await GetBearerToken(siteUrl, userName, password);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(siteUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token.AccessToken);

            return await client.GetStringAsync(apiController);
        }

        static internal async Task<TokenResponseModel> GetBearerToken(string siteUrl, string userName, string password)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(siteUrl);
                client.DefaultRequestHeaders.Clear();

                HttpContent requestContent = new StringContent($"grant_type=password&username={userName}&password={password}", Encoding.UTF8, "application/x-www-form-urlencoded");

                HttpResponseMessage responseMessage = await client.PostAsync("Token", requestContent);

                if (!responseMessage.IsSuccessStatusCode)
                {
                    return null;
                }
                using (Stream responseStream = await responseMessage.Content.ReadAsStreamAsync())
                {
                    return (TokenResponseModel)JsonConvert.DeserializeObject(new StreamReader(responseStream).ReadToEnd(), typeof(TokenResponseModel));
                }

            }
        }
    }
}
