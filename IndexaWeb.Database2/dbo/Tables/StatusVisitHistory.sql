﻿CREATE TABLE [dbo].[StatusVisitHistory]
(
	[StatusVisitHistoryId] INT NOT NULL IDENTITY PRIMARY KEY, 
    [StatusVisitId] INT NOT NULL, 
    [VisitHeaderId] INT NOT NULL, 
    [Comment] NVARCHAR(MAX) NULL, 
	[Active]		BIT	NOT NULL DEFAULT 1,
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [CreatedDate] DATETIME NOT NULL,
	[ModifiedBy] NVARCHAR(50) NULL,
	[ModifiedDate] DATETIME NULL
)
