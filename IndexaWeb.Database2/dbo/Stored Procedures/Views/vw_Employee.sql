﻿CREATE VIEW vw_Employee 
AS 
SELECT ISNULL(Person.PersonId,-1) as PersonId,
       Name ,
       LastName ,
       Phone ,
       Email ,
       Login ,
       Password ,
       Person.CreatedBy ,
       Person.CreatedDate ,
       Person.ModifiedBy ,
       Person.ModifiedDate ,
       EmployeeId ,
       EmployeeLevel ,
       SupervisorId ,
       Employee.CreatedBy AS EmployeeCreatedBy,
       Employee.CreatedDate AS EmployeeCreatedDate,
       Employee.ModifiedBy AS EmployeeModifiedBy,
       Employee.ModifiedDate AS EmployeeModifiedDate
FROM dbo.Person 
INNER JOIN dbo.Employee ON Employee.PersonId = Person.PersonId
GO
CREATE TRIGGER tr_CreateEmployee
ON dbo.vw_Employee
INSTEAD OF INSERT
AS
BEGIN
	DECLARE @NewEmployeeId INT
	INSERT INTO dbo.Person (Name,LastName,Phone,Email,Login,Password,CreatedBy)
	SELECT Inserted.Name,Inserted.LastName,Inserted.Phone,Inserted.Email,Inserted.Login,Inserted.Password,Inserted.CreatedBy
	FROM Inserted
	
	SET @NewEmployeeId =SCOPE_IDENTITY()

	INSERT INTO dbo.Employee
	        ( 
	          PersonId ,
	          EmployeeLevel ,
	          SupervisorId,
			  CreatedBy
	        )
	SELECT  @NewEmployeeId ,
            Inserted.EmployeeLevel ,
            Inserted.SupervisorId,
			Inserted.CreatedBy

	FROM Inserted
END