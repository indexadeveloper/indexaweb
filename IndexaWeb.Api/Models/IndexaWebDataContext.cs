﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IndexaWeb.Api.Models
{
    public class IndexaWebDataContext : DbContext
    {
        public IndexaWebDataContext(): base("name=IndexaWebContext")
        {

        }

        //public virtual DbSet<C__RefactorLog> C__RefactorLog { get; set; }
        //public virtual DbSet<Activity> Activity { get; set; }
        public virtual DbSet<User> Users { get; set; }
        //public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        //public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        //public virtual DbSet<Customer> Customer { get; set; }
        //public virtual DbSet<CustomersPerEmployee> CustomersPerEmployee { get; set; }
        //public virtual DbSet<Employee> Employee { get; set; }
        //public virtual DbSet<EmployeeLevel> EmployeeLevel { get; set; }
        //public virtual DbSet<Level> Level { get; set; }
        //public virtual DbSet<Location> Location { get; set; }
        ////public virtual DbSet<Person> Person { get; set; }
        ////public virtual DbSet<ProjectHeaders> ProjectHeaders { get; set; }
        //public virtual DbSet<ProjectTask> ProjectTask { get; set; }
        //public virtual DbSet<Reclaim> Reclaim { get; set; }
        //public virtual DbSet<ReclaimComment> ReclaimComment { get; set; }
        //public virtual DbSet<Representative> Representative { get; set; }
        //public virtual DbSet<StatusTask> StatusTask { get; set; }
        //public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        //public virtual DbSet<VisitDetail> VisitDetail { get; set; }
        //public virtual DbSet<VisitHeader> VisitHeader { get; set; }
        //public virtual DbSet<StatusVisit> StatusVisit { get; set; }
        //public virtual DbSet<StatusVisitHistory> StatusVisitHistory { get; set; }
    }
}