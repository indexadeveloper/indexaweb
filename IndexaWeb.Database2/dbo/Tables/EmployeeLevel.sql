﻿CREATE TABLE [dbo].[EmployeeLevel]
(
	[EmployeeLevelId]	INT NOT NULL IDENTITY PRIMARY KEY,
	[EmployeeId]		INT NOT NULL,
	[LevelId]			INT NOT NULL, 
	[Active]		BIT	NOT NULL DEFAULT 1,
    CONSTRAINT [FK_EmployeeLevel_ToEmployee] FOREIGN KEY ([EmployeeId]) REFERENCES [Employee]([EmployeeId]), 
    CONSTRAINT [FK_EmployeeLevel_ToLevel] FOREIGN KEY ([LevelId]) REFERENCES [Level]([LevelId])
)
