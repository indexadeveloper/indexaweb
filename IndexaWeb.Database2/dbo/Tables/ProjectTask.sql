﻿CREATE TABLE [dbo].[ProjectTask] (
    [ProjectHeaderId] INT            NULL,
    [ProjectTaskId]   INT            IDENTITY (1, 1) NOT NULL,
    [TaskName]        NVARCHAR (100) NULL,
    [PublicationDate] DATETIME2        NOT NULL,
    [StartingDate]    DATETIME2       NULL,
    [EstimatedEnding] DATETIME2       NULL,
    [EndDate]         DATETIME2       NULL,
    [TaskObservation] NVARCHAR (500) NULL,
    [StatusTaskId]    INT            NULL,
    [AssignedToUser] NVARCHAR(50) NULL, 
    [Hours] INT NULL, 
    PRIMARY KEY CLUSTERED ([ProjectTaskId] ASC)
);

