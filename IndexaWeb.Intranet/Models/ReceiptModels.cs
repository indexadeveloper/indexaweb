﻿using System.ComponentModel.DataAnnotations;

namespace IndexaWeb.Intranet.Models
{
     
    public class ReceiptModels
    {
        public int CODNOM { get; set; }
        public decimal VALOR { get; set; }
        public decimal MONTO { get; set; }
        public char IMPDT { get; set; }
        public int ANIO { get; set; }
        public int MES { get; set; }
        public string DESCRIP { get; set; }
        public decimal MONTOBASE { get; set; }
        public int CEDULA { get; set; }
        public decimal SALDOPRE { get; set; }
        public string APENOM { get; set; }
        public decimal SUESAL { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
    }
}