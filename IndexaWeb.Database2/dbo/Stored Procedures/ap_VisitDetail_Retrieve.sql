﻿CREATE PROC ap_VisitDetail_Retrieve
@VisitDetailId int=NULL
,@VisitHeaderid int=NULL
,@ActivityId int=NULL
,@VisitNotes nvarchar (500)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[VisitDetailId]
,[VisitHeaderid]
,[ActivityId]
,[VisitNotes]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM VisitDetail
WHERE
 ([VisitDetailId] = @VisitDetailId OR @VisitDetailId IS NULL)
AND ([VisitHeaderid] = @VisitHeaderid OR @VisitHeaderid IS NULL)
AND ([ActivityId] = @ActivityId OR @ActivityId IS NULL)
AND ([VisitNotes] = @VisitNotes OR @VisitNotes IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
