namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            CustomersPerEmployee = new HashSet<CustomersPerEmployee>();
            Location = new HashSet<Location>();
            Representative = new HashSet<Representative>();
            VisitHeader = new HashSet<VisitHeader>();
        }
        [Key]
        public int CustomerId { get; set; }

        [StringLength(50)]
        [Display(Name="RIF")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Rif { get; set; }

        [StringLength(50)]
        [Display(Name = "Cliente")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Alias")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Alias { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [StringLength(50)]
        [Display(Name = "Creado por")]
        public string CreatedBy { get; set; }

        [Display(Name = "Creado El")]
        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado por")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomersPerEmployee> CustomersPerEmployee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Location> Location { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Representative> Representative { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitHeader> VisitHeader { get; set; }
        
    }
}
