﻿CREATE PROC ap_Aplications_Retrieve
@Id nvarchar (128)=NULL
,@Name nvarchar (MAX)=NULL
AS
SELECT 
[Id]
,[Name]
FROM Aplications
WHERE
 ([Id] = @Id OR @Id IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
