﻿CREATE PROC ap_ProjectHeader_Update
@ProjectHeaderId int=NULL
,@CustomerId int=NULL
AS
UPDATE ProjectHeader 
SET 
[CustomerId]=ISNULL(@CustomerId,CustomerId)
WHERE
 [ProjectHeaderId]=@ProjectHeaderId
