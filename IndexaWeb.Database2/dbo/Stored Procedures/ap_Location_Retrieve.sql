﻿CREATE PROC ap_Location_Retrieve
@LocationId int=NULL
,@Description nvarchar (50)=NULL
,@CustomerId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[LocationId]
,[Description]
,[CustomerId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM Location
WHERE
 ([LocationId] = @LocationId OR @LocationId IS NULL)
AND ([Description] = @Description OR @Description IS NULL)
AND ([CustomerId] = @CustomerId OR @CustomerId IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
