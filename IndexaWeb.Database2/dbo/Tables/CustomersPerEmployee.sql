﻿CREATE TABLE [dbo].[CustomersPerEmployee] (
    [CustomersPerEmployeeId] INT          IDENTITY (1, 1) NOT NULL,
    [EmployeeId]             INT          NULL,
    [CustomerId]             INT          NULL,
    [CreatedBy]              VARCHAR (50) NULL,
    [CreatedDate]            DATETIME     DEFAULT (getdate()) NULL,
    [ModifiedBy]             VARCHAR (50) NULL,
    [ModifiedDate]           DATETIME     NULL,
    CONSTRAINT [PK_CustomersPerEmployee] PRIMARY KEY CLUSTERED ([CustomersPerEmployeeId] ASC),
    CONSTRAINT [FK_CustomersPerEmployee_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_CustomersPerEmployee_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

