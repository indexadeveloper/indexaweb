﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.Models
{
    public class Names
    {
        [Display(Name = "Proyecto")]
        public string ProjectName { get; set; }
        [Display(Name = "Tarea")]
        public string ProjectTaskName { get; set; }
        [Display(Name = "Estatus")]
        public string EstatusName { get; set; }
        [Display(Name = "Fecha de cambio")]
        public DateTime ChangeDate { get; set; }
    }
}