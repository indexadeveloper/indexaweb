﻿CREATE TABLE [dbo].[Employee] (
    [EmployeeId]    INT          IDENTITY (1, 1) NOT NULL,
    [UserName]      NVARCHAR(128)          NOT NULL,
    [SupervisorId]  INT          NULL,
	[Active]		BIT	NOT NULL DEFAULT 1,
    [CreatedBy]     VARCHAR (50) NOT NULL,
    [CreatedDate]   DATETIME      NOT NULL,
    [ModifiedBy]    VARCHAR (50) NULL,
    [ModifiedDate]  DATETIME     NULL,
    [PersonId] VARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([EmployeeId] ASC)
);

