﻿CREATE TABLE [dbo].[ProjectHeader] (
    [ProjectHeaderId] INT IDENTITY (1, 1) NOT NULL,
    [CustomerId]      INT NOT NULL,
    [Name] NVARCHAR(50) NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [CreatedDate] DATETIME NOT NULL, 
    [ModifiedBy] NVARCHAR(50) NULL, 
    [ModifiedDate] DATETIME NULL, 
    PRIMARY KEY CLUSTERED ([ProjectHeaderId] ASC), 
    CONSTRAINT [FK_ProjectHeader_ToTable] FOREIGN KEY ([CustomerId]) REFERENCES [Customer]([CustomerId])
);

