﻿CREATE PROC ap_ReclaimComment_Retrieve
@ReclaimCommentId int=NULL
,@Description nvarchar (500)=NULL
,@ReclaimId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[ReclaimCommentId]
,[Description]
,[ReclaimId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM ReclaimComment
WHERE
 ([ReclaimCommentId] = @ReclaimCommentId OR @ReclaimCommentId IS NULL)
AND ([Description] = @Description OR @Description IS NULL)
AND ([ReclaimId] = @ReclaimId OR @ReclaimId IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
