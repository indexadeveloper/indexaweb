﻿using IndexaWeb.Intranet.App_Start;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IndexaWeb.Intranet.Controllers
{
    public class ReceiptController : Controller
    {
        RRHH db = new RRHH();
        IndexaWebModel dbi = new IndexaWebModel();
        // GET: Receipt
        public ActionResult Index(string userName)
        {
            ViewBag.Ci = dbi.Employee.Where(x => x.UserName == userName).First().PersonId;
            return View();
        }

        [HttpPost]
        public void Generar(string hasta, string desde, string ci)
        {
            DataTable receipt = db.obtenerRecibos(ci, Convert.ToDateTime(desde), Convert.ToDateTime(hasta));
            GenerarPdf nuevo = new GenerarPdf();
            List<Recibos> ListReceipt = new List<Recibos>();
            List<List<string>> recibos = new List<List<string>>();
            List<string> prestaciones = new List<string>();

            PdfPTable Error = new PdfPTable(30);
            PdfPCell cellAux = new PdfPCell(new Phrase("No existen recibos para esa fecha", new Font(Font.NORMAL, 12f, Font.BOLD)));
            cellAux.Colspan = 30;
            cellAux.Border = 0;
            cellAux.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            Error.AddCell(cellAux);

            try
            {
                string oldNomrecibo = "";
                for (int i = 0; i <= receipt.Rows.Count-1; i++)
                {
                    List<string> quincena = new List<string>();
                    quincena.Add(receipt.Rows[i]["CEDULA"].ToString());
                    quincena.Add(receipt.Rows[i]["APENOM"].ToString());
                    quincena.Add(receipt.Rows[i]["DFECING"].ToString().Replace("12:00:00 a. m.", ""));
                    quincena.Add(receipt.Rows[i]["SUESAL"].ToString());
                    quincena.Add(receipt.Rows[i]["DES_CAR"].ToString());
                    quincena.Add(receipt.Rows[i]["DFECHAPAGO"].ToString().Replace("12:00:00 a. m.", ""));
                    quincena.Add(receipt.Rows[i]["DPERIODO_FIN"].ToString().Replace("12:00:00 a. m.", ""));
                    oldNomrecibo = receipt.Rows[i]["NOMRECIBO"].ToString();
                    quincena.Add(receipt.Rows[i]["NOMRECIBO"].ToString());

                    while (i < receipt.Rows.Count  && receipt.Rows[i]["NOMRECIBO"].ToString() == oldNomrecibo)
                    {
                        if (receipt.Rows[i]["TIPCON"].ToString() != "P")
                        {
                            quincena.Add(receipt.Rows[i]["DESCRIP"].ToString());
                            if(receipt.Rows[i]["VALOR"].ToString() != receipt.Rows[i]["MONTO"].ToString() && receipt.Rows[i]["VALOR"].ToString() != "0,00")
                                quincena.Add(receipt.Rows[i]["VALOR"].ToString());
                            else
                                quincena.Add(" ");
                            quincena.Add(receipt.Rows[i]["MONTO"].ToString());
                            quincena.Add(receipt.Rows[i]["TIPCON"].ToString());
                        }
                        oldNomrecibo = receipt.Rows[i]["NOMRECIBO"].ToString();
                        i++; 
                    }
                    recibos.Add(quincena);
                    i--;
                }

                ListReceipt = Generar_Recibos(recibos);
            
                nuevo.f_generarPDF(ListReceipt, "Recibos de pago.pdf", Error);
            }catch(Exception e)
            { 
                nuevo.f_generarPDF(ListReceipt, "Recibos de pago.pdf", Error);
            }
        }

        public List<Recibos> Generar_Recibos(List<List<string>> recibos)
        {
            List<Recibos> listReceipt = new List<Recibos>();
            foreach (var item in recibos)
            {
                decimal asignaciones = 0, deducciones = 0;
                Recibos re = new Recibos();
                PdfPTable datos = new PdfPTable(30);
                PdfPCell cell = new PdfPCell(new Phrase("INDEXA IT SOLUTIONS, C.A.", new Font(Font.NORMAL, 12f, Font.BOLD)));
                cell.Colspan = 30;
                cell.Border = 0;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("J-31644285-3                                                          Fecha:" + item[5].ToString(), new Font(Font.NORMAL, 8f)));
                cell.Colspan = 30;
                cell.Border = 0;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(item[7].ToString(), new Font(Font.NORMAL, 12f, Font.BOLD)));
                cell.Colspan = 30;
                cell.Border = 0;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Cedula", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 5;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Apellido y Nombre", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 20;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Fecha de Ingreso", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 5;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(item[0].ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 5;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);


                cell = new PdfPCell(new Phrase(item[1].ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 20;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);


                cell = new PdfPCell(new Phrase(item[2].ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 5;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Sueldo Mensual: ", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 7;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(item[3].ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 7;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Cargo:", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 3;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(item[4].ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 13;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Concepto", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 12;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Referencia", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Asignación", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Deducción", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                for (int i = 8; i < item.Count-1; i++)
                {
                    cell = new PdfPCell(new Phrase(item[i].ToString(), new Font(Font.NORMAL, 10f)));
                    cell.Colspan = 12;
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    datos.AddCell(cell);

                    if(item[i+3].ToString() == "A")
                    { 
                        cell = new PdfPCell(new Phrase(item[i+1].ToString(), new Font(Font.NORMAL, 10f)));
                        cell.Colspan = 6;
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        datos.AddCell(cell);

                        cell = new PdfPCell(new Phrase(item[i+2].ToString(), new Font(Font.NORMAL, 10f)));
                        cell.Colspan = 6;
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        datos.AddCell(cell);
                        asignaciones = asignaciones + Convert.ToDecimal(item[i + 2].ToString());

                        cell = new PdfPCell(new Phrase(" ", new Font(Font.NORMAL, 10f)));
                        cell.Colspan = 6;
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        datos.AddCell(cell);
                    }
                    else if(item[i+3].ToString() == "D")
                    {
                        cell = new PdfPCell(new Phrase(item[i + 1].ToString(), new Font(Font.NORMAL, 10f)));
                        cell.Colspan = 6;
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        datos.AddCell(cell);

                        cell = new PdfPCell(new Phrase(" ", new Font(Font.NORMAL, 10f)));
                        cell.Colspan = 6;
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        datos.AddCell(cell);
                        
                        cell = new PdfPCell(new Phrase(item[i+2].ToString(), new Font(Font.NORMAL, 10f)));
                        cell.Colspan = 6;
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        datos.AddCell(cell);
                        deducciones = deducciones + Convert.ToDecimal(item[i + 2].ToString());
                    }

                    i = i+3;
                }

                cell = new PdfPCell(new Phrase(" "));
                cell.Colspan = 30;
                cell.Border = 0;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Total Asignaciones y Deducciones", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 18;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(asignaciones.ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(deducciones.ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Periodo 1/" + (Convert.ToDateTime(item[6].ToString()).Month) + "/" + (Convert.ToDateTime(item[6].ToString()).Year) + "  AL  " + new DateTime(Convert.ToDateTime(item[6].ToString()).Year, Convert.ToDateTime(item[6].ToString()).Month, DateTime.DaysInMonth(Convert.ToDateTime(item[6].ToString()).Year, Convert.ToDateTime(item[6].ToString()).Month)).ToString().Replace("12:00:00 a. m.", ""), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 18;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Neto a Cobrar:", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase((asignaciones - deducciones).ToString(), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                Numalet let = new Numalet();
                let.MascaraSalidaDecimal = "00'/100'";

                if (let.ToCustomString((asignaciones - deducciones).ToString(), true)[0] == 'u' && let.ToCustomString((asignaciones - deducciones).ToString(), true)[1] == 'n' && let.ToCustomString((asignaciones - deducciones).ToString(), true)[2] == ' ')
                    cell = new PdfPCell(new Phrase("Son " + let.ToCustomString((asignaciones - deducciones).ToString(), true).Replace("un ", ""), new Font(Font.NORMAL, 10f)));
                else
                    cell = new PdfPCell(new Phrase("Son " + let.ToCustomString((asignaciones - deducciones).ToString(), true), new Font(Font.NORMAL, 10f)));
                cell.Colspan = 30;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(" "));
                cell.Colspan = 30;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Utilidades Acumuladas:", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 12;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("3000", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 7;
                cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(" ", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 11;
                cell.Border = 0;
                cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("INDEXA IT SOLUTIONS, C.A. Serial Nro. ENTP247342362411928", new Font(Font.NORMAL, 6f)));
                cell.Colspan = 17;
                cell.Border = 0;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("_______________", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 13;
                cell.Border = 0;
                cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase(" ", new Font(Font.NORMAL, 6f)));
                cell.Colspan = 17;
                cell.Border = 0;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("Recibi Conforme", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 13;
                cell.Border = 0;
                cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);

                cell = new PdfPCell(new Phrase("------------------------------------------------------------------------------------------------------------------------------------------", new Font(Font.NORMAL, 10f)));
                cell.Colspan = 30;
                cell.Border = 0;
                cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                datos.AddCell(cell);
                re.setRecibo(datos, Convert.ToDateTime(item[6]).Month);
                listReceipt.Add(re);
            }
            return listReceipt;
        }
    }
}