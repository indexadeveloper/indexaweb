﻿CREATE PROC ap_Permissions_Update
@Id int=NULL
,@Name nvarchar (MAX)=NULL
AS
UPDATE Permissions 
SET 
[Name]=ISNULL(@Name,Name)
WHERE
 [Id]=@Id
