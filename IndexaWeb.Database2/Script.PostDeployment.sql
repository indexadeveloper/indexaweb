﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
SET IDENTITY_INSERT [dbo].[StatusTask] ON
INSERT INTO [dbo].[StatusTask] ([StatusTaskID], [Name]) VALUES (1, N'Nueva')
INSERT INTO [dbo].[StatusTask] ([StatusTaskID], [Name]) VALUES (2, N'Iniciado')
INSERT INTO [dbo].[StatusTask] ([StatusTaskID], [Name]) VALUES (3, N'Finalizado')
INSERT INTO [dbo].[StatusTask] ([StatusTaskID], [Name]) VALUES (4, N'Anulado')
SET IDENTITY_INSERT [dbo].[StatusTask] OFF
SET IDENTITY_INSERT [dbo].[Level] ON
INSERT INTO [dbo].[Level] ([LevelId], [Name], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Gerente', N'Admin', N'2016-05-06 08:54:52', NULL, NULL)
INSERT INTO [dbo].[Level] ([LevelId], [Name], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Supervisor Desarrollo', N'Admin', N'2016-05-06 08:55:08', NULL, NULL)
INSERT INTO [dbo].[Level] ([LevelId], [Name], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Supervisor Soporte', N'Admin', N'2016-05-06 08:55:22', NULL, NULL)
INSERT INTO [dbo].[Level] ([LevelId], [Name], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Desarrollador', N'Admin', N'2016-05-06 08:55:38', NULL, NULL)
INSERT INTO [dbo].[Level] ([LevelId], [Name], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'Soporte', N'Admin', N'2016-05-06 08:55:53', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Level] OFF
SET IDENTITY_INSERT [dbo].[StatusVisit] ON
INSERT INTO [dbo].[StatusVisit] ([StatusVisitId], [Name]) VALUES (1, N'Nueva')
INSERT INTO [dbo].[StatusVisit] ([StatusVisitId], [Name]) VALUES (2, N'Pendiente por Aprobar')
INSERT INTO [dbo].[StatusVisit] ([StatusVisitId], [Name]) VALUES (3, N'Aprobada')
INSERT INTO [dbo].[StatusVisit] ([StatusVisitId], [Name]) VALUES (4, N'Desaprobada')
SET IDENTITY_INSERT [dbo].[StatusVisit] OFF
SET IDENTITY_INSERT [dbo].[Customer] ON
INSERT INTO [dbo].[Customer] ([CustomerId], [Rif], [Name], [Alias], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'J-00014864-3', N'Ford Motors Company', N'Ford', N'Admin', N'2016-05-16 14:03:26', NULL, NULL)
INSERT INTO [dbo].[Customer] ([CustomerId], [Rif], [Name], [Alias], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'J-30714575-7', N'Comercializadora Kromi Market CA', N'Kromi Market', N'Admin', N'2016-05-18 14:06:44', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET IDENTITY_INSERT [dbo].[Location] ON
INSERT INTO [dbo].[Location] ([LocationId], [Description], [CustomerId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Prebo', 2, N'Admin', N'2016-05-18 14:07:03', NULL, NULL)
INSERT INTO [dbo].[Location] ([LocationId], [Description], [CustomerId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'Mañongo', 2, N'Admin', N'2016-05-18 15:20:20', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Location] OFF
SET IDENTITY_INSERT [dbo].[Activity] ON
INSERT INTO [dbo].[Activity] ([ActivityId], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Limpiar Servidor', N'Admin', N'2016-05-23 00:00:00', NULL, NULL)
INSERT INTO [dbo].[Activity] ([ActivityId], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Revisar Conexiones', N'Admin', N'2016-05-23 00:00:00', NULL, NULL)
INSERT INTO [dbo].[Activity] ([ActivityId], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Otros', N'Admin', N'2016-05-23 00:00:00', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Activity] OFF