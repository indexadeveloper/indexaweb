﻿CREATE PROC ap_Employee_Retrieve
@EmployeeId int=NULL
,@UserName int=NULL
,@SupervisorId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[EmployeeId]
,[UserName]
,[SupervisorId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM Employee
WHERE
 ([EmployeeId] = @EmployeeId OR @EmployeeId IS NULL)
AND ([UserName] = @UserName OR @UserName IS NULL)
AND ([SupervisorId] = @SupervisorId OR @SupervisorId IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
