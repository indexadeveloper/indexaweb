﻿namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Table("EmployeeLevel")]
    public class EmployeeLevel
    {
        public EmployeeLevel()
        {
        }
        [Key]
        public int EmployeeLevelId { get; set; }
        public int EmployeeId { get; set; }
        public int LevelId { get; set; }

        public virtual Level Level { get; set; }
        public virtual Employee Employee { get; set; } 
        
    }
}
