﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;

namespace IndexaWeb.Intranet.Controllers
{
    public class ReclaimCommentsController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        // GET: ReclaimComments
        public async Task<ActionResult> Index()
        {
            var reclaimComment = db.ReclaimComment.Include(r => r.Reclaim);
            return View(await reclaimComment.ToListAsync());
        }

        // GET: ReclaimComments/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReclaimComment reclaimComment = await db.ReclaimComment.FindAsync(id);
            if (reclaimComment == null)
            {
                return HttpNotFound();
            }
            return View(reclaimComment);
        }

        // GET: ReclaimComments/Create
        public ActionResult Create()
        {
            ViewBag.ReclaimId = new SelectList(db.Reclaim, "ReclaimId", "Description");
            return View();
        }

        // POST: ReclaimComments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ReclaimCommentId,Description,ReclaimId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] ReclaimComment reclaimComment)
        {
            if (ModelState.IsValid)
            {
                db.ReclaimComment.Add(reclaimComment);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ReclaimId = new SelectList(db.Reclaim, "ReclaimId", "Description", reclaimComment.ReclaimId);
            return View(reclaimComment);
        }

        // GET: ReclaimComments/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReclaimComment reclaimComment = await db.ReclaimComment.FindAsync(id);
            if (reclaimComment == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReclaimId = new SelectList(db.Reclaim, "ReclaimId", "Description", reclaimComment.ReclaimId);
            return View(reclaimComment);
        }

        // POST: ReclaimComments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ReclaimCommentId,Description,ReclaimId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] ReclaimComment reclaimComment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reclaimComment).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ReclaimId = new SelectList(db.Reclaim, "ReclaimId", "Description", reclaimComment.ReclaimId);
            return View(reclaimComment);
        }

        // GET: ReclaimComments/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReclaimComment reclaimComment = await db.ReclaimComment.FindAsync(id);
            if (reclaimComment == null)
            {
                return HttpNotFound();
            }
            return View(reclaimComment);
        }

        // POST: ReclaimComments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ReclaimComment reclaimComment = await db.ReclaimComment.FindAsync(id);
            db.ReclaimComment.Remove(reclaimComment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
