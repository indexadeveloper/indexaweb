﻿CREATE PROC ap_Permissions_Retrieve
@Id int=NULL
,@Name nvarchar (MAX)=NULL
AS
SELECT 
[Id]
,[Name]
FROM Permissions
WHERE
 ([Id] = @Id OR @Id IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
