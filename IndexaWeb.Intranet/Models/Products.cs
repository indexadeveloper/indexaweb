﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.Models
{
    [Table("Products")]
    public class Products
    {
        [Key]
        public int Id { get; set; }
        public int ProjectHeadersId { get; set; }
        [Display(Name = "Codigo")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Code { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Description { get; set; }
        [Display(Name = "Serial")]
        public string Serial { get; set; }
    }
}