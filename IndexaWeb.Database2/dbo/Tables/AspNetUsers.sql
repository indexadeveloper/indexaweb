﻿CREATE TABLE [dbo].[AspNetUsers] (
    [Id]					NVARCHAR (128) NOT NULL,
    [UserName]				NVARCHAR (MAX) NOT NULL,
    [PasswordHash]			NVARCHAR (MAX) NULL,
    [SecurityStamp]			NVARCHAR (MAX) NULL,
    [Discriminator]			NVARCHAR (128) NULL,
	[Name]					NVARCHAR (MAX) NOT NULL,
	[LastName]				NVARCHAR (MAX) NOT NULL,
	[Email]					NVARCHAR (128) NOT NULL,
	[EmailConfirmed]		BIT NOT NULL,
	[PhoneNumber]				NVARCHAR (128) NULL,
	[PhoneNumberConfirmed]		BIT NOT NULL,
	[TwoFactorEnabled]		BIT NULL,
	[LockoutEndDateUtc]		DATETIME NULL,
	[LockoutEnabled]		BIT NULL,
	[AccessFailedCount]		INT NOT NULL DEFAULT 0,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

