﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;

namespace IndexaWeb.Intranet.Controllers
{
    public class SpendingHistoriesController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        // GET: SpendingHistories
        public async Task<ActionResult> Index(int? id, string name)
        {
            ViewBag.idp = id;
            ViewBag.name = name;
            ViewBag.total = 0;
            var spendingHistories = db.SpendingHistories.Include(s => s.Projectheaders);
            return View(await db.SpendingHistories.Where(x => x.ProjectHeaderId == id).ToListAsync());
        }

        // GET: SpendingHistories/Details/5
        public async Task<ActionResult> Details(int? id, int? idp, string name)
        {
            ViewBag.idp = idp;
            ViewBag.name = name;
            ViewBag.id = id;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingHistory spendingHistory = await db.SpendingHistories.FindAsync(id);
            if (spendingHistory == null)
            {
                return HttpNotFound();
            }
            return View(spendingHistory);
        }

        // GET: SpendingHistories/Create
        public ActionResult Create(int? idp, string name)
        {
            ViewBag.idp = idp;
            ViewBag.name = name;
            ViewBag.ProjectHeaderId = new SelectList(db.ProjectHeader, "ProjectHeaderId", "Name");
            return View();
        }

        // POST: SpendingHistories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SpendingHistoryId,Reference,Cost,ProjectHeaderId")] SpendingHistory spendingHistory, int idp, string name)
        {
            ViewBag.idp = idp;
            ViewBag.name = name;
            spendingHistory.ProjectHeaderId = idp;
            if (ModelState.IsValid)
            {
                db.SpendingHistories.Add(spendingHistory);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new { id = idp, name = name });
            }

            ViewBag.ProjectHeaderId = new SelectList(db.ProjectHeader, "ProjectHeaderId", "Name", spendingHistory.ProjectHeaderId);
            return View(spendingHistory);
        }

        // GET: SpendingHistories/Edit/5
        public async Task<ActionResult> Edit(int? id, int? idp, string name)
        {
            ViewBag.idp = idp;
            ViewBag.name = name;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingHistory spendingHistory = await db.SpendingHistories.FindAsync(id);
            if (spendingHistory == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProjectHeaderId = new SelectList(db.ProjectHeader, "ProjectHeaderId", "Name", spendingHistory.ProjectHeaderId);
            return View(spendingHistory);
        }

        // POST: SpendingHistories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SpendingHistoryId,Reference,Cost,ProjectHeaderId")] SpendingHistory spendingHistory, int id, int idp, string name)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spendingHistory).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new { id = idp, name = name });
            }
            ViewBag.ProjectHeaderId = new SelectList(db.ProjectHeader, "ProjectHeaderId", "Name", spendingHistory.ProjectHeaderId);
            return View(spendingHistory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
