namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Location")]
    public partial class Location
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Location()
        {
            VisitHeader = new HashSet<VisitHeader>();
        }

        [Key]
        public int LocationId { get; set; }

        [StringLength(50)]
        [Display(Name = "Sucursal")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Description { get; set; }

        [Display(Name = "Cliente")]
        public int CustomerId { get; set; }

        [StringLength(50)]
        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }

        [Display(Name = "Creado El")]
        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }
        
        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitHeader> VisitHeader { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{

        //}
    }
}
