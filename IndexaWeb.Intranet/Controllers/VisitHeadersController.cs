﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Validation;
using System.Net.Mail;
using System.Net.Mime;

namespace IndexaWeb.Intranet.Controllers
{
    [Authorize]
    public class VisitHeadersController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        // GET: VisitHeaders
        public async Task<ActionResult> Index()
        {
            var visitHeader = db.VisitHeader.Include(v => v.Customer).Include(v => v.Employee).Include(v => v.Location).Include(s => s.StatusVisit);
            if (User.IsInRole("Soporte"))
                visitHeader = visitHeader.Where(v => v.Employee.UserName == User.Identity.Name);
            if (User.IsInRole("Cliente"))
                visitHeader = visitHeader.Where(v => v.Customer.Representative.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().CustomerId == v.CustomerId);
            return View(await visitHeader.Where(v => v.Active == true).ToListAsync());
        }
        
        public ActionResult GetLocationsByCustomerId(int id)
        {
            var locations = db.Location.Where(m => m.CustomerId == id).Select(p=> new { LocationId = p.LocationId, Description = p.Description });
            return Json(locations.ToList(), JsonRequestBehavior.AllowGet);
        }
        // GET: VisitHeaders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitHeader visitHeader = await db.VisitHeader.FindAsync(id);
            if (visitHeader == null)
            {
                return HttpNotFound();
            }
            ViewBag.StatusVisit = visitHeader.StatusVisitId;
            ViewBag.ReclaimExists = visitHeader.Reclaims.Count > 0;
            if (ViewBag.ReclaimExists)
                visitHeader.Reclaims = db.Reclaim.Where(r => r.VisitHeaderId == visitHeader.VisitHeaderId).ToList();
            ViewBag.VisitHeaderId = visitHeader.VisitHeaderId;
            ViewBag.isAdminOrCustomer = User.IsInRole("Administrador") || User.IsInRole("Cliente");
            if (visitHeader.CreatedBy == User.Identity.Name || ViewBag.isAdminOrCustomer)
                return View(visitHeader);
            return RedirectToAction("Index");
        }

        private void SaveStatusVisitHistory(int newStatus, int visitHeaderId)
        {
            StatusVisitHistory statusVisitHistory = new StatusVisitHistory();
            statusVisitHistory.StatusVisitId = newStatus;
            statusVisitHistory.VisitHeaderId = visitHeaderId;
            statusVisitHistory.CreatedBy = User.Identity.Name;
            statusVisitHistory.CreatedDate = DateTime.Now;
            db.StatusVisitHistory.Add(statusVisitHistory);
            db.SaveChanges();
        }

        

        public string GetIP4Address()
        {
            string IP4Address = String.Empty;

            foreach (IPAddress ipa in Dns.GetHostAddresses(Request.ServerVariables["REMOTE_ADDR"].ToString()))
            {
                if (ipa.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = ipa.ToString();
                    break;
                }
            }

            if (IP4Address != String.Empty)
            {
                return IP4Address;
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            return IP4Address;
        }

        public void SendConfirmationMail(VisitHeader visitHeader)
        {
            //string htmlBody = "<img src = '~/Content/images/logo.jpg' />";
            //AlternateView avHtml = AlternateView.CreateAlternateViewFromString
            //   (htmlBody, null, MediaTypeNames.Text.Html);

            //LinkedResource inline = new LinkedResource(Server.MapPath("/IndexaWeb.Intranet/Content/images/logo.jpg"), MediaTypeNames.Image.Jpeg);
            //inline.ContentId = Guid.NewGuid().ToString();
            //avHtml.LinkedResources.Add(inline);


            string supervisorMailAddress = (from j in db.Employee
                                            join k in db.AspNetUsers
                                            on j.UserName equals k.UserName
                                            where j.EmployeeId == visitHeader.Employee.SupervisorId
                                            select k.Email).FirstOrDefault();
            List<string> representativesMailAddresses = (from j in db.Representative
                                                     join k in db.AspNetUsers
                                                     on j.UserName equals k.UserName
                                                     where j.CustomerId == visitHeader.CustomerId
                                                     select k.Email).ToList();
            
            MailMessage mail = new MailMessage();
            Attachment att = new Attachment(Server.MapPath("/IndexaWeb.Intranet/Content/images/logo.jpg"));
            //mail.AlternateViews.Add(avHtml);
            mail.Attachments.Add(att);
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = true;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Credentials = new NetworkCredential("mperez@indexa.com.ve", "N1c0l3.,");
            mail.From = new MailAddress("mperez@indexa.com.ve");
            foreach (var email in representativesMailAddresses)
            {
                mail.To.Add(new MailAddress(email));
            }
            if (supervisorMailAddress != null)
                mail.To.Add(new MailAddress(supervisorMailAddress));
            mail.To.Add(new MailAddress(visitHeader.Employee.Email));
            mail.Subject = "Confirmación de visita";
            mail.IsBodyHtml = true;
            mail.Body = "<p>Buen día, por medio de la presente el soportista " + visitHeader.Employee.EmployeeName + " indica que ha realizado una visita en </ br>" +
                " su empresa, por favor verifique la información de la visita en el siguiente <a href='192.168.100.2/IndexaWeb/VisitHeaders/Details/" + visitHeader.VisitHeaderId + "'>enlace</a></ br>" +
                "</ br>Esto es una Prueba</p></ br>"+
                "Control de Visitas INDEXA IT Solutions</p>" +
                "<footer class='panel-footer'>" +
                "<p>" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + "</ p>" +
                "</ footer >";
            client.Send(mail);

        }

        // GET: VisitHeaders/Create
        [Authorize(Order = 1, Roles = "Administrador, Soporte")]
        public ActionResult Create()
        {
            //ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name");
            ViewBag.LocationId = new SelectList(db.Location, "LocationId", "Description");
            return View();
        }
        // POST: VisitHeaders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Order = 1, Roles = "Administrador, Soporte")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CustomerId,LocationId,VisitDate,ArrivalTime,DepatureTime")] VisitHeader visitHeader)
        {
            
            if (ModelState.IsValid)
            {
                visitHeader.EmployeeId = db.Employee.Where(e => e.UserName == User.Identity.Name).Select(e => e.EmployeeId).FirstOrDefault();
                visitHeader.StatusVisitId = 1;
                visitHeader.IpAddress = GetIP4Address();
                visitHeader.Active = true;
                visitHeader.CreatedBy = User.Identity.Name;
                visitHeader.CreatedDate = DateTime.Now;
                try
                {
                    db.VisitHeader.Add(visitHeader);
                    await db.SaveChangesAsync();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var error in e.EntityValidationErrors)
                    {
                        foreach (var thisError in error.ValidationErrors)
                        {
                            ModelState.AddModelError(thisError.PropertyName, thisError.ErrorMessage);
                        }
                    }
                    //ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name", visitHeader.CustomerId);
                    ViewBag.LocationId = new SelectList(db.Location, "LocationId", "Description", visitHeader.LocationId);
                    return View(visitHeader);
                }      
                SaveStatusVisitHistory(visitHeader.StatusVisitId, visitHeader.VisitHeaderId);

                return RedirectToAction("Edit", new { id = visitHeader.VisitHeaderId });
            }

            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name", visitHeader.CustomerId);
            ViewBag.LocationId = new SelectList(db.Location, "LocationId", "Description", visitHeader.LocationId);
            return View(visitHeader);
        }

        [Authorize(Order = 1, Roles = "Administrador, Soporte")]
        // GET: VisitHeaders/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            VisitHeader visitHeader = await db.VisitHeader.FindAsync(id);
            if (visitHeader == null)
            {
                return HttpNotFound();
            }
            ViewBag.VisitHeaderId = id;
            ViewBag.VisitDateYear = visitHeader.VisitDate.Year;
            ViewBag.VisitDateMonth = visitHeader.VisitDate.Month;
            ViewBag.VisitDateDay = visitHeader.VisitDate.Day;
            ViewBag.SelectedLocationId = visitHeader.LocationId;
            ViewBag.ErrorMessage = "";
            ViewBag.Activities = new SelectList(db.Activity.ToList(), "ActivityId", "Description");
            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name", visitHeader.CustomerId);
            ViewBag.EmployeeId = new SelectList(db.Employee, "EmployeeId", "CreatedBy", visitHeader.EmployeeId);
            ViewBag.LocationId = new SelectList(db.Location, "LocationId", "Description", visitHeader.LocationId);
            if ((visitHeader.CreatedBy == User.Identity.Name || User.IsInRole("Administrador")) && visitHeader.StatusVisitId == 1)
                return View(visitHeader);
            return RedirectToAction("Index");
        }

        // POST: VisitHeaders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Order = 1, Roles = "Administrador, Soporte")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "VisitHeaderId,EmployeeId,CustomerId,LocationId,VisitDate,ArrivalTime,DepatureTime")] VisitHeader visitHeader)
        {
            ViewBag.ErrorMessage = "";
            if (ModelState.IsValid)
            {
                VisitHeader visit = db.VisitHeader.Find(visitHeader.VisitHeaderId);
                if (visit.VisitDetail.Count > 0)
                {
                    visit.CustomerId = visitHeader.CustomerId;
                    visit.LocationId = visitHeader.LocationId;
                    visit.VisitDate = visitHeader.VisitDate;
                    visit.ArrivalTime = visitHeader.ArrivalTime;
                    visit.DepatureTime = visitHeader.DepatureTime;
                    visit.StatusVisitId = 2;
                    visit.ModifiedBy = User.Identity.Name;
                    visit.ModifiedDate = DateTime.Now;
                    db.Entry(visit).State = EntityState.Modified;
                    await db.SaveChangesAsync();

                    SaveStatusVisitHistory(visitHeader.StatusVisitId, visitHeader.VisitHeaderId);
                    SendConfirmationMail(visit);
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = "No puede confirmar una visita sin actividades";
                }
            }
            ViewBag.VisitHeaderId = visitHeader.VisitHeaderId;
            ViewBag.SelectedLocationId = visitHeader.LocationId;
            ViewBag.SelectedVisitDate = visitHeader.VisitDate;
            ViewBag.Activities = new SelectList(db.Activity.ToList(), "ActivityId", "Description");
            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name", visitHeader.CustomerId);
            ViewBag.EmployeeId = new SelectList(db.Employee, "EmployeeId", "CreatedBy", visitHeader.EmployeeId);
            ViewBag.LocationId = new SelectList(db.Location, "LocationId", "Description", visitHeader.LocationId);
            return View(visitHeader);
        }
        [Authorize(Order = 1, Roles = "Administrador,Cliente")]
        [HttpGet]
        public async Task<ActionResult> ConfirmVisit(int id)
        {
            VisitHeader visitHeader = await db.VisitHeader.FindAsync(id);
            if (visitHeader == null)
            {
                return HttpNotFound();
            }
            ViewBag.StatusVisit = new SelectList(db.StatusVisit.Where(s => s.StatusVisitId > 2).ToList(), "StatusVisitId", "Name");
            ViewBag.isAdminOrCustomer = User.IsInRole("Administrador") || User.IsInRole("Cliente");
            if (!ViewBag.isAdminOrCustomer && visitHeader.StatusVisitId > 1)
                return RedirectToAction("Index");
            //ViewBag.VisitDetail = new VisitDetail
            //{
            //    VisitHeaderId = id
            //};
            return View(visitHeader);
        }



        [Authorize(Order = 1, Roles = "Administrador,Cliente")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmVisit([Bind(Include = "VisitHeaderId,EmployeeId,CustomerId,LocationId,VisitDate,ArrivalTime,DepatureTime,StatusVisitId")]VisitHeader visitHeader)
        {
            if (ModelState.IsValid)
            {
                VisitHeader visit = db.VisitHeader.Find(visitHeader.VisitHeaderId);
                visit.StatusVisitId = visitHeader.StatusVisitId;
                visit.ModifiedBy = User.Identity.Name;
                visit.ModifiedDate = DateTime.Now;
                db.Entry(visit).State = EntityState.Modified;
                await db.SaveChangesAsync();

                SaveStatusVisitHistory(visit.StatusVisitId, visit.VisitHeaderId);

                if (visit.StatusVisit.Name != "Desaprobada")
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Details", new { id = visit.VisitHeaderId });
            }

            ViewBag.StatusVisit = new SelectList(db.StatusVisit.ToList(), "StatusVisitId", "Name");
            //ViewBag.VisitDetail = new VisitDetail
            //{
            //    VisitHeaderId = visitHeader.VisitHeaderId
            //};
            return View(visitHeader);
        }

        public ActionResult AddReclaim(int id)
        {
            Reclaim reclaim = new Reclaim
            {
                VisitHeaderId = id
            };
            return PartialView("_CreateReclaim", reclaim);
        }
        [HttpPost]
        public async Task<ActionResult> AddReclaim(Reclaim reclaim)
        {
            reclaim.Active = true;
            reclaim.CreatedBy = User.Identity.Name;
            reclaim.CreatedDate = DateTime.Now;
            try
            {
                db.Reclaim.Add(reclaim);
                await db.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Details", new { id = reclaim.VisitHeaderId });
        }

        [Authorize(Roles = "Administrador")]
        // GET: VisitHeaders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitHeader visitHeader = await db.VisitHeader.FindAsync(id);
            if (visitHeader == null)
            {
                return HttpNotFound();
            }
            return View(visitHeader);
        }

        [Authorize(Roles = "Administrador")]
        // POST: VisitHeaders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            VisitHeader visitHeader = await db.VisitHeader.FindAsync(id);
            visitHeader.Active = false;
            visitHeader.ModifiedBy = User.Identity.Name;
            visitHeader.ModifiedDate = DateTime.Now;
            db.Entry(visitHeader).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw;
            }
            return RedirectToAction("Index");
        }

        public ActionResult AddVisitDetail(int id)
        {
            VisitDetail visitDetail = new VisitDetail
            {
                VisitHeaderId = id
            };
            ViewBag.Activities = new SelectList(db.Activity.ToList(), "ActivityId", "Description");
            return PartialView("_CreateVisitDetails", visitDetail);
        }
        [HttpPost]
        public async Task<ActionResult> AddVisitDetail(VisitDetail visitDetail)
        {
            visitDetail.CreatedBy = User.Identity.Name;
            visitDetail.CreatedDate = DateTime.Now;
            try
            {
                db.VisitDetail.Add(visitDetail);
                await db.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            ViewBag.Activities = new SelectList(db.Activity.ToList(), "ActivityId", "Description");
            return RedirectToAction("Edit", new { id = visitDetail.VisitHeaderId });
        }
        public ActionResult AddReclaimComment(int id)
        {
            Reclaim reclaim = db.Reclaim.Where(r => r.VisitHeaderId == id).FirstOrDefault();
            if (reclaim == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            ReclaimComment comment = new ReclaimComment
            {
                ReclaimId = reclaim.ReclaimId
            };
            return PartialView("_CreateReclaimComment", comment);
        }
        [HttpPost]
        public async Task<ActionResult> AddReclaimComment(ReclaimComment comment)
        {
            comment.CreatedBy = User.Identity.Name;
            comment.CreatedDate = DateTime.Now;
            try
            {
                db.ReclaimComment.Add(comment);
                await db.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
            var visitHeaderId = db.Reclaim.Find(comment.ReclaimId).VisitHeaderId;
            return RedirectToAction("Details", "VisitHeaders", new { id = visitHeaderId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
