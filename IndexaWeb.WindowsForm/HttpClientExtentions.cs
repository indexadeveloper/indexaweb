﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IndexaWeb.WindowsForm
{
    public static class HttpClientExtentions
    {
        public static AuthenticationHeaderValue ToHeaderValue(this string username, string password)
        {
            return new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}")));
        }
    }
}
