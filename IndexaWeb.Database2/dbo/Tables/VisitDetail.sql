﻿CREATE TABLE [dbo].[VisitDetail] (
    [VisitDetailId] INT            IDENTITY (1, 1) NOT NULL,
    [VisitHeaderId] INT            NOT NULL,
    [ActivityId]    INT            NOT NULL,
	[TicketNumber]	INT			   NULL,
    [VisitNotes]    NVARCHAR (500) NULL,
	[Active]		BIT	NOT NULL DEFAULT 1,
    [CreatedBy]     VARCHAR (50)   NOT NULL,
    [CreatedDate]   DATETIME        NOT NULL,
    [ModifiedBy]    VARCHAR (50)   NULL,
    [ModifiedDate]  DATETIME       NULL,
    CONSTRAINT [PK_VisitDetail] PRIMARY KEY CLUSTERED ([VisitDetailId] ASC),
    CONSTRAINT [FK_VisitDetail_VisitHeader] FOREIGN KEY ([VisitHeaderId]) REFERENCES [dbo].[VisitHeader] ([VisitHeaderId]), 
    CONSTRAINT [FK_VisitDetail_Activity] FOREIGN KEY ([ActivityId]) REFERENCES [Activity]([ActivityId])
);

