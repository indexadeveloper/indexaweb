﻿CREATE PROC ap_ElementPermission_Retrieve
@Element_Id int=NULL
,@Permissions_Id int=NULL
AS
SELECT 
[Element_Id]
,[Permissions_Id]
FROM ElementPermission
WHERE
 ([Element_Id] = @Element_Id OR @Element_Id IS NULL)
AND ([Permissions_Id] = @Permissions_Id OR @Permissions_Id IS NULL)
