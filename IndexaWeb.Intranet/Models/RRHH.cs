﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for Modulo
/// </summary>
public class RRHH
{
    private string cs;
    public string error { get; private set; }
    public int lastId { get; private set; }
    public RRHH()
    {
        this.cs = ConfigurationManager.ConnectionStrings["RRHH"].ToString();
        this.error = string.Empty;
        this.lastId = 0;
    }
    private void reset()
    {
        this.error = string.Empty;
        this.lastId = 0;
    }
    public DataTable obtenerRecibos(string cedula, DateTime desde, DateTime hasta)
    {
        var dt = new DataTable();
        SqlConnection conn = new SqlConnection(this.cs);
        this.reset();
        try
        {
            SqlDataAdapter tabla = new SqlDataAdapter("SELECT l.[CODNOM], k.[DES_CAR], l.[DFECHAPAGO], l.[DFECING], l.[VALOR], l.[DPERIODO_FIN], l.[MONTO], l.[ANIO], l.[MES], l.[DESCRIP], l.[TIPCON], l.[IMPDET], l.[NOMRECIBO], l.[MONTOBASE], l.[CEDULA], l.[SALDOPRE], l.[APENOM], l.[SUESAL], l.[NOMBRES], l.[APELLIDOS] FROM(SELECT m.[CODNOM], w.[CODCARGO], w.[DFECING], w.[VALOR], m.[DFECHAPAGO], m.[DPERIODO_FIN], w.[MONTO], w.[ANIO], w.[MES], w.[DESCRIP], w.[TIPCON], w.[IMPDET], m.[DESCRIP] AS 'NOMRECIBO', w.[MONTOBASE], w.[CEDULA], w.[SALDOPRE], w.[APENOM], w.[SUESAL], w.[NOMBRES], w.[APELLIDOS] FROM(SELECT x.[CODNOM], x.[VALOR], x.[MONTO], x.[TIPCON], x.[IMPDET], x.[ANIO], x.[MES], x.[DESCRIP], x.[MONTOBASE], x.[CEDULA], x.[SALDOPRE], y.[APENOM], y.[CODCARGO], y.[SUESAL], y.[NOMBRES], y.[APELLIDOS], y.[DFECING] FROM[SWNOMMSSQL000001].[dbo].[swnommdt] x INNER JOIN[SWNOMMSSQL000001].[dbo].[swnomper] y ON x.[CEDULA] = y.[CEDULA]) w INNER JOIN[SWNOMMSSQL000001].[dbo].[swnomhmv] m ON w.[CODNOM] = m.[CODNOM]) l INNER JOIN[SWNOMMSSQL].[dbo].[swnomcar] k ON l.[CODCARGO] = k.[COD_CAR] WHERE l.[CEDULA] = '" + cedula + "' AND l.[DPERIODO_FIN] BETWEEN '"+ Convert.ToDateTime(desde).ToString("yyyyMMdd") + "' AND '"+ Convert.ToDateTime(hasta).ToString("yyyyMMdd") + "' AND l.[DFECHAPAGO] < GETDATE() ORDER BY l.[NOMRECIBO] ASC", conn);
            tabla.Fill(dt);
        }
        catch (Exception ex)
        {
            this.error = ex.Message;
            dt = null;
        }
        finally
        {
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        return dt;
    }
}