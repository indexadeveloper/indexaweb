﻿using IndexaWeb.WindowsForm.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IndexaWeb.WindowsForm
{
    public static class TokenResponse
    {
        public static async Task<string> GetWebApiRequest(string siteUrl, string apiController, string userName, string password)
        {
            TokenResponseModel Token = await GetBearerToken(siteUrl, userName, password);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(siteUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token.AccessToken);

            return await client.GetStringAsync(apiController);
        }

        public static async Task<TokenResponseModel> GetBearerToken(string siteUrl, string userName, string password)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(siteUrl);
                client.DefaultRequestHeaders.Clear();

                HttpContent requestContent = new StringContent($"grant_type=password&username={userName}&password={password}", Encoding.UTF8, "application/x-www-form-urlencoded");

                HttpResponseMessage responseMessage = await client.PostAsync("Token", requestContent);

                if (!responseMessage.IsSuccessStatusCode)
                {
                    return null;
                }
                using (Stream responseStream = await responseMessage.Content.ReadAsStreamAsync())
                {
                    return (TokenResponseModel)JsonConvert.DeserializeObject(new StreamReader(responseStream).ReadToEnd(), typeof(TokenResponseModel));
                }

            }
        }
    }
}
