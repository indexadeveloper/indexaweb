﻿CREATE PROC ap_Customer_Retrieve
@CustomerId int=NULL
,@Rif nvarchar (50)=NULL
,@Name nvarchar (50)=NULL
,@Alias nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[CustomerId]
,[Rif]
,[Name]
,[Alias]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM Customer
WHERE
 ([CustomerId] = @CustomerId OR @CustomerId IS NULL)
AND ([Rif] = @Rif OR @Rif IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
AND ([Alias] = @Alias OR @Alias IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
