﻿CREATE PROC ap_Person_Insert
@Name nvarchar (50)=NULL
,@LastName nvarchar (50)=NULL
,@Phone nvarchar (50)=NULL
,@Email nvarchar (50)=NULL
,@Login nvarchar (50)=NULL
,@Password nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
INSERT INTO Person( 
[Name]
,[LastName]
,[Phone]
,[Email]
,[Login]
,[Password]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
)
VALUES ( 
@Name
,@LastName
,@Phone
,@Email
,@Login
,@Password
,@CreatedBy
,@CreatedDate
,@ModifiedBy
,@ModifiedDate
)
SELECT SCOPE_IDENTITY() AS Id
