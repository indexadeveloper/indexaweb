﻿SET IDENTITY_INSERT [dbo].[Employee] ON
INSERT INTO [dbo].[Employee] ([EmployeeId], [UserName], [SupervisorId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'rafaeldao', NULL, N'rafaeldao', N'2016-05-26 08:39:40', NULL, NULL)
INSERT INTO [dbo].[Employee] ([EmployeeId], [UserName], [SupervisorId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'rojo2', 1, N'rojo2', N'2016-05-26 09:03:08', NULL, NULL)
INSERT INTO [dbo].[Employee] ([EmployeeId], [UserName], [SupervisorId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Admin', 1, N'Admin', N'2016-06-10 09:28:06', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Employee] OFF
