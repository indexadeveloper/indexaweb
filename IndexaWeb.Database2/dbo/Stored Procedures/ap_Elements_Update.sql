﻿CREATE PROC ap_Elements_Update
@Id int=NULL
,@Name nvarchar (MAX)=NULL
,@Aplications_Id nvarchar (128)=NULL
AS
UPDATE Elements 
SET 
[Name]=ISNULL(@Name,Name)
,[Aplications_Id]=ISNULL(@Aplications_Id,Aplications_Id)
WHERE
 [Id]=@Id
