﻿CREATE PROC ap_CustomersPerEmployee_Insert
@EmployeeId int=NULL
,@CustomerId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
INSERT INTO CustomersPerEmployee( 
[EmployeeId]
,[CustomerId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
)
VALUES ( 
@EmployeeId
,@CustomerId
,@CreatedBy
,@CreatedDate
,@ModifiedBy
,@ModifiedDate
)
SELECT SCOPE_IDENTITY() AS Id
