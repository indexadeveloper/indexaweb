﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.Models
{
    [Table("SpendingHistory")]
    public class SpendingHistory
    {
        [Key]
        [Display(Name = "Nro")]
        public int SpendingHistoryId { get; set; }
        [Required(ErrorMessage = "El campo es obligatorio")]
        [Display(Name = "Referencia")]
        public string Reference { get; set; }
        [Required(ErrorMessage = "El campo es obligatorio")]
        [Display(Name = "Costo")]
        public decimal Cost { get; set; }
        [Display(Name = "Proyecto")]
        public int ProjectHeaderId { get; set; }
        public virtual ProjectHeaders Projectheaders { get; set; }
    }
}