﻿CREATE PROC ap_Person_Retrieve
@PersonId int=NULL
,@Name nvarchar (50)=NULL
,@LastName nvarchar (50)=NULL
,@Phone nvarchar (50)=NULL
,@Email nvarchar (50)=NULL
,@Login nvarchar (50)=NULL
,@Password nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[PersonId]
,[Name]
,[LastName]
,[Phone]
,[Email]
,[Login]
,[Password]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM Person
WHERE
 ([PersonId] = @PersonId OR @PersonId IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
AND ([LastName] = @LastName OR @LastName IS NULL)
AND ([Phone] = @Phone OR @Phone IS NULL)
AND ([Email] = @Email OR @Email IS NULL)
AND ([Login] = @Login OR @Login IS NULL)
AND ([Password] = @Password OR @Password IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
