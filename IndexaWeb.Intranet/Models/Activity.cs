﻿namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Table("Activity")]
    public class Activity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Activity()
        {
            VisitDetail = new HashSet<VisitDetail>();
        }
        [Key]
        public int ActivityId { get; set; }
        [Display(Name = "Descripción")]
        public string Description { get; set; }
        
        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [StringLength(128)]
        [Display(Name = "Creado por")]
        public string CreatedBy { get; set; }

        [Display(Name = "Creado El")]
        public DateTime CreatedDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Modificado por")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitDetail> VisitDetail { get; set; }
    }
}
