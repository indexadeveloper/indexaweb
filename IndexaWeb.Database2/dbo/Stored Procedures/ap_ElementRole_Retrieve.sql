﻿CREATE PROC ap_ElementRole_Retrieve
@Element_Id int=NULL
,@Roles_Id int=NULL
AS
SELECT 
[Element_Id]
,[Roles_Id]
FROM ElementRole
WHERE
 ([Element_Id] = @Element_Id OR @Element_Id IS NULL)
AND ([Roles_Id] = @Roles_Id OR @Roles_Id IS NULL)
