﻿CREATE PROC ap_ProjectTask_Retrieve
@ProjectHeaderId int=NULL
,@ProjectTaskId int=NULL
,@TaskName nvarchar (100)=NULL
,@PublicationDate datetime=NULL
,@StartingDate datetime=NULL
,@EstimatedEnding datetime=NULL
,@EndDate datetime=NULL
,@TaskObservation nvarchar (500)=NULL
,@StatusTaskId int=NULL
AS
SELECT 
[ProjectHeaderId]
,[ProjectTaskId]
,[TaskName]
,[PublicationDate]
,[StartingDate]
,[EstimatedEnding]
,[EndDate]
,[TaskObservation]
,[StatusTaskId]
FROM ProjectTask
WHERE
 ([ProjectHeaderId] = @ProjectHeaderId OR @ProjectHeaderId IS NULL)
AND ([ProjectTaskId] = @ProjectTaskId OR @ProjectTaskId IS NULL)
AND ([TaskName] = @TaskName OR @TaskName IS NULL)
AND ([PublicationDate] = @PublicationDate OR @PublicationDate IS NULL)
AND ([StartingDate] = @StartingDate OR @StartingDate IS NULL)
AND ([EstimatedEnding] = @EstimatedEnding OR @EstimatedEnding IS NULL)
AND ([EndDate] = @EndDate OR @EndDate IS NULL)
AND ([TaskObservation] = @TaskObservation OR @TaskObservation IS NULL)
AND ([StatusTaskId] = @StatusTaskId OR @StatusTaskId IS NULL)
