﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IndexaWeb.Intranet.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string Name { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // EF won't let us swap out IdentityUserRole for ApplicationUserRole here:         
            modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id).Property(p => p.Name).IsRequired();
            modelBuilder.Entity<ApplicationUser>().HasMany<IdentityUserRole>((ApplicationUser u) => u.Roles);
            modelBuilder.Entity<IdentityUserRole>().HasKey((IdentityUserRole r) =>
                new { UserId = r.UserId, RoleId = r.RoleId }).ToTable("AspNetUserRoles");
            modelBuilder.Entity<IdentityUserLogin>().HasKey(u => new { u.UserId, u.LoginProvider, u.ProviderKey });


            modelBuilder.Entity<IdentityUser>()
                .ToTable("AspNetUsers", "dbo");
            modelBuilder.Entity<ApplicationUser>()
                .ToTable("AspNetUsers", "dbo");
            //modelBuilder.Entity<IdentityUserRole>()
            //    .ToTable("AspNetUserRoles", "dbo");
            modelBuilder.Entity<IdentityUserLogin>()
                .ToTable("AspNetUserLogins", "dbo");

            modelBuilder.Entity<IdentityUserClaim>()
                .ToTable("AspNetUserClaims", "dbo");
            modelBuilder.Entity<IdentityRole>()
                .ToTable("AspNetRoles", "dbo");
        }

        public System.Data.Entity.DbSet<IndexaWeb.Intranet.AspNetUsers> IdentityUsers { get; set; }
        
    }
}