namespace IndexaWeb.Intranet
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class AspNetRoles : IdentityRole
    {
        public AspNetRoles() : base()
        {
        }

        public AspNetRoles(string name) : base(name) { }
        
    }
}
