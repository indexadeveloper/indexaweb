namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Reclaim")]
    public partial class Reclaim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Reclaim()
        {
            ReclaimComment = new HashSet<ReclaimComment>();
        }
        [Key]
        public int ReclaimId { get; set; }

        [StringLength(100)]
        [Display(Name = "Reclamo")]
        public string Description { get; set; }

        public int VisitHeaderId { get; set; }

        public bool Active { get; set; }

        [StringLength(50)]
        [Display(Name = "Realizado Por")]
        public string CreatedBy { get; set; }

        [Display(Name = "Fecha del Reclamo")]
        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }

        public virtual VisitHeader VisitHeader { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReclaimComment> ReclaimComment { get; set; }
    }
}
