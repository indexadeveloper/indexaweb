namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Web.Mvc;

    [Table("Representative")]
    public partial class Representative
    {
        private IndexaWebModel db = new IndexaWebModel();
        public Representative()
        {
        }
        [Key]
        public int RepresentativeId { get; set; }

        [Display(Name = "Nombre de Usuario")]
        public string UserName { get; set; }

        [Display(Name = "Nombre")]
        public string RepresentativeName
        {
            get
            {
                return db.AspNetUsers.Where(u => u.UserName == this.UserName).Select(x => x.Name + " " + x.LastName).FirstOrDefault();
            }
        }
        [Display(Name = "Email")]
        public string Email
        {
            get
            {
                return db.AspNetUsers.Where(u => u.UserName == this.UserName).Select(x => x.Email).FirstOrDefault();
            }
        }
        [Display(Name = "Tel�fono")]
        public string PhoneNumber
        {
            get
            {
                return db.AspNetUsers.Where(u => u.UserName == this.UserName).Select(x => x.PhoneNumber).FirstOrDefault();
            }
        }
        [Display(Name = "Cliente")]
        public int CustomerId { get; set; }


        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }

        [Display(Name = "Creado El")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }

        [Display(Name = "Cliente")]
        public virtual Customer Customer { get; set; }

        [Display(Name = "Nombre de Usuario")]
        public IEnumerable<SelectListItem> UserNames { get; set; }
    }
}
