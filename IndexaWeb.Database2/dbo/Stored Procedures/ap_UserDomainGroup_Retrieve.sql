﻿CREATE PROC ap_UserDomainGroup_Retrieve
@Groups_Id int=NULL
,@UserDomains_Id int=NULL
AS
SELECT 
[Groups_Id]
,[UserDomains_Id]
FROM UserDomainGroup
WHERE
 ([Groups_Id] = @Groups_Id OR @Groups_Id IS NULL)
AND ([UserDomains_Id] = @UserDomains_Id OR @UserDomains_Id IS NULL)
