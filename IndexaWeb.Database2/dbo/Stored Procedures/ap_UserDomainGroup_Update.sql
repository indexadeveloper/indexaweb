﻿CREATE PROC ap_UserDomainGroup_Update
@Groups_Id int=NULL
,@UserDomains_Id int=NULL
AS
UPDATE UserDomainGroup 
SET 
[Groups_Id]=ISNULL(@Groups_Id,Groups_Id)
,[UserDomains_Id]=ISNULL(@UserDomains_Id,UserDomains_Id)
WHERE
 [Groups_Id]=@Groups_Id
AND [UserDomains_Id]=@UserDomains_Id
