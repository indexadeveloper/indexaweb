﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;
using Microsoft.AspNet.Identity;

namespace IndexaWeb.Intranet.Controllers
{
    [Authorize]
    public class EmployeesController : Controller
    {
        private ApplicationDbContext context;
        private IndexaWebModel db;

        public EmployeesController()
        {
            context = new ApplicationDbContext();
            db = new IndexaWebModel();
        }

        public class EmployeeName
        {
            public int? EmployeeId { get; set; }
            public string Name { get; set; }
        }

        // GET: Employees
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Index()
        {
            var employee = db.Employee;
            return View(await employee.Where(e => e.Active).ToListAsync());
        }

        // GET: Employees/Details/5
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.Employee.FindAsync(id.Value);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }
        // GET: Employees/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(string userName = "")
        {
            var supers = (from s in db.Employee
                          join a in db.AspNetUsers
                          on s.UserName equals a.UserName
                          join e in db.EmployeeLevel
                          on s.EmployeeId equals e.EmployeeId
                          where e.LevelId <= 3
                          select new EmployeeName
                          {
                              EmployeeId = s.EmployeeId,
                              Name = a.Name + " " + a.LastName
                          }).Distinct().ToList();

            Employee model = new Employee
            {
                EmployeeLevels = new SelectList(db.Level.ToList(), "LevelId", "Name",false),
                Supervisors = new SelectList(supers, "EmployeeId", "Name"),
                UserNames = new SelectList(db.AspNetUsers.ToList(),"UserName","UserName", userName)
            };
            return View(model);
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Employee employeeModel)
        {
            if (Request.QueryString["FromRegister"] != null)
            {
                if (User.Identity.IsAuthenticated)
                {
                    string userName = User.Identity.Name;
                    Employee employee = new Employee();
                    if (ModelState.IsValid)
                    {
                        using (var connection = db.Database.BeginTransaction())
                        {
                            try
                            {
                                employee.UserName = userName;
                                employee.SupervisorId = employeeModel.SupervisorId;
                                employee.Active = true;
                                employee.CreatedBy = User.Identity.Name;
                                employee.CreatedDate = DateTime.Now;
                                employee.PersonId = employeeModel.PersonId;


                                db.Employee.Add(employee);
                                await db.SaveChangesAsync();
                                foreach (var item in employeeModel.SelectedLevels)
                                {
                                    EmployeeLevel employeeLevel = new EmployeeLevel();
                                    employeeLevel.LevelId = Convert.ToInt32(item);
                                    employeeLevel.EmployeeId = employee.EmployeeId;
                                    employee.EmployeeLevel.Add(employeeLevel);
                                    await db.SaveChangesAsync();
                                }
                                connection.Commit();
                            }
                            catch (Exception)
                            {
                                connection.Rollback();
                                throw;
                            }

                        }
                        return RedirectToAction("Index");
                    }
                }
            }
            else
            {
                Employee employee = new Employee();
                if (ModelState.IsValid)
                {

                    employee.UserName = employeeModel.UserName;
                    employee.SupervisorId = employeeModel.SupervisorId;
                    employee.Active = true;
                    employee.CreatedBy = User.Identity.Name;
                    employee.CreatedDate = DateTime.Now;
                    employee.PersonId = employeeModel.PersonId;
                    using (var connection = db.Database.BeginTransaction())
                    {
                        try
                        {
                            db.Employee.Add(employee);
                            await db.SaveChangesAsync();
                            foreach (var item in employeeModel.SelectedLevels)
                            {
                                EmployeeLevel employeeLevel = new EmployeeLevel();
                                employeeLevel.LevelId = Convert.ToInt32(item);
                                employeeLevel.EmployeeId = employee.EmployeeId;
                                employee.EmployeeLevel.Add(employeeLevel);
                                await db.SaveChangesAsync();
                            }
                            connection.Commit();
                        }
                        catch (Exception)
                        {
                            connection.Rollback();
                            throw;
                        }
                    }

                    return RedirectToAction("Index");
                }
            }
            return View(employeeModel);
        }
        /// <summary>
        /// Resolve Employee Edit View
        /// </summary>
        /// <param name="id">Identifier of Employee</param>
        /// <returns></returns>
        // GET: Employees/Edit/5
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.Employee.FindAsync(id);
            var levels = db.Level.ToList();
            employee.SelectedLevels = db.EmployeeLevel.Where(x => x.EmployeeId == employee.EmployeeId).Select(y => y.LevelId).Distinct().ToList();
            employee.EmployeeLevels = new SelectList(levels, "LevelId", "Name");
            employee.Supervisors = new SelectList((from s in db.Employee
                                                  join a in db.AspNetUsers
                                                  on s.UserName equals a.UserName
                                                  join e in db.EmployeeLevel
                                                  on s.EmployeeId equals e.EmployeeId
                                                  where e.LevelId <= 3 && e.EmployeeId != id
                                                  select new EmployeeName
                                                  {
                                                      EmployeeId = s.EmployeeId,
                                                      Name = a.Name + " " + a.LastName
                                                  }).Distinct().ToList(), "EmployeeId", "Name");
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Edit(Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.ModifiedBy = User.Identity.Name;
                employee.ModifiedDate = DateTime.Now;
                try
                {
                    db.Entry(employee).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    foreach (var level in db.EmployeeLevel.Where(x => x.EmployeeId == employee.EmployeeId).ToArray())
                    {
                        db.EmployeeLevel.Remove(level);
                    }
                    foreach (var item in employee.SelectedLevels)
                    {
                        EmployeeLevel employeeLevel = new EmployeeLevel();
                        employeeLevel.LevelId = Convert.ToInt32(item);
                        employeeLevel.EmployeeId = employee.EmployeeId;
                        employee.EmployeeLevel.Add(employeeLevel);
                        await db.SaveChangesAsync();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction("Index");
            }
            
            return View(employee);
        }
        [Authorize(Roles = "Administrador")]
        // GET: Employees/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await db.Employee.FindAsync(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }
        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Employee employee = await db.Employee.FindAsync(id);
            employee.Active = false;
            employee.ModifiedBy = User.Identity.Name;
            employee.ModifiedDate = DateTime.Now;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
