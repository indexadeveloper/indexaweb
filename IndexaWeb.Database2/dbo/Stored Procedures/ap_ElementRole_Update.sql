﻿CREATE PROC ap_ElementRole_Update
@Element_Id int=NULL
,@Roles_Id int=NULL
AS
UPDATE ElementRole 
SET 
[Element_Id]=ISNULL(@Element_Id,Element_Id)
,[Roles_Id]=ISNULL(@Roles_Id,Roles_Id)
WHERE
 [Element_Id]=@Element_Id
AND [Roles_Id]=@Roles_Id
