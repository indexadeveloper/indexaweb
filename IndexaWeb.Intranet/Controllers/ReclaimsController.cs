﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;

namespace IndexaWeb.Intranet.Controllers
{
    public class ReclaimsController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        // GET: Reclaims
        public async Task<ActionResult> Index()
        {
            var reclaims = db.Reclaim.Include(r => r.VisitHeader);
            return View(await reclaims.ToListAsync());
        }

        public async Task<ActionResult> GetReclaimByVisit(int visitHeaderId)
        {
            var reclaimByVisit = db.Reclaim.Where(r => r.VisitHeaderId == visitHeaderId);
            return View(await reclaimByVisit.FirstOrDefaultAsync());
        }

        // GET: Reclaims/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reclaim reclaim = await db.Reclaim.FindAsync(id);
            if (reclaim == null)
            {
                return HttpNotFound();
            }
            return View(reclaim);
        }

        // GET: Reclaims/Create
        public ActionResult Create()
        {
            ViewBag.VisitHeaderId = new SelectList(db.VisitHeader, "VisitHeaderId", "CreatedBy");
            return View();
        }

        // POST: Reclaims/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ReclaimId,Description,VisitHeaderId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] Reclaim reclaim)
        {
            if (ModelState.IsValid)
            {
                db.Reclaim.Add(reclaim);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.VisitHeaderId = new SelectList(db.VisitHeader, "VisitHeaderId", "CreatedBy", reclaim.VisitHeaderId);
            return View(reclaim);
        }

        // GET: Reclaims/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reclaim reclaim = await db.Reclaim.FindAsync(id);
            if (reclaim == null)
            {
                return HttpNotFound();
            }
            ViewBag.VisitHeaderId = new SelectList(db.VisitHeader, "VisitHeaderId", "CreatedBy", reclaim.VisitHeaderId);
            return View(reclaim);
        }

        // POST: Reclaims/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ReclaimId,Description,VisitHeaderId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] Reclaim reclaim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reclaim).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.VisitHeaderId = new SelectList(db.VisitHeader, "VisitHeaderId", "CreatedBy", reclaim.VisitHeaderId);
            return View(reclaim);
        }

        // GET: Reclaims/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reclaim reclaim = await db.Reclaim.FindAsync(id);
            if (reclaim == null)
            {
                return HttpNotFound();
            }
            return View(reclaim);
        }

        // POST: Reclaims/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Reclaim reclaim = await db.Reclaim.FindAsync(id);
            db.Reclaim.Remove(reclaim);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
