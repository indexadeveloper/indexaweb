﻿using System.ComponentModel.DataAnnotations;

namespace IndexaWeb.Intranet.Models
{
    public class StatusTaskModels
    {
        [Key]
        public int Id { get; set; }
        public string ProjectName { get; set; }
    }
}