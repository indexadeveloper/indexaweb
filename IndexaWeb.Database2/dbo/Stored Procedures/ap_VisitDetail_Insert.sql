﻿CREATE PROC ap_VisitDetail_Insert
@VisitHeaderid int=NULL
,@ActivityId int=NULL
,@VisitNotes nvarchar (500)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
INSERT INTO VisitDetail( 
[VisitHeaderid]
,[ActivityId]
,[VisitNotes]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
)
VALUES ( 
@VisitHeaderid
,@ActivityId
,@VisitNotes
,@CreatedBy
,@CreatedDate
,@ModifiedBy
,@ModifiedDate
)
SELECT SCOPE_IDENTITY() AS Id
