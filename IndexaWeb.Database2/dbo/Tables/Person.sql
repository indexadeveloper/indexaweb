﻿CREATE TABLE [dbo].[Person] (
    [PersonId]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (50) NULL,
    [LastName]     NVARCHAR (50) NULL,
    [Phone]        NVARCHAR (50) NULL,
    [Email]        NVARCHAR (50) NULL,
    [Login]        NVARCHAR (50) NULL,
    [Password]     NVARCHAR (50) NULL,
    [CreatedBy]    VARCHAR (50)  NULL,
    [CreatedDate]  DATETIME      DEFAULT (getdate()) NULL,
    [ModifiedBy]   VARCHAR (50)  NULL,
    [ModifiedDate] DATETIME      NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

