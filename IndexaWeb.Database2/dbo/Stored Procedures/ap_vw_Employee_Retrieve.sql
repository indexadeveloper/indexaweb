﻿CREATE PROC ap_vw_Employee_Retrieve
@PersonId int=NULL
,@Name nvarchar (50)=NULL
,@LastName nvarchar (50)=NULL
,@Phone nvarchar (50)=NULL
,@Email nvarchar (50)=NULL
,@Login nvarchar (50)=NULL
,@Password nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
,@EmployeeId int=NULL
,@EmployeeLevel int=NULL
,@SupervisorId int=NULL
,@EmployeeCreatedBy varchar (50)=NULL
,@EmployeeCreatedDate datetime=NULL
,@EmployeeModifiedBy varchar (50)=NULL
,@EmployeeModifiedDate datetime=NULL
AS
SELECT 
[PersonId]
,[Name]
,[LastName]
,[Phone]
,[Email]
,[Login]
,[Password]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
,[EmployeeId]
,[EmployeeLevel]
,[SupervisorId]
,[EmployeeCreatedBy]
,[EmployeeCreatedDate]
,[EmployeeModifiedBy]
,[EmployeeModifiedDate]
FROM vw_Employee
WHERE
 ([PersonId] = @PersonId OR @PersonId IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
AND ([LastName] = @LastName OR @LastName IS NULL)
AND ([Phone] = @Phone OR @Phone IS NULL)
AND ([Email] = @Email OR @Email IS NULL)
AND ([Login] = @Login OR @Login IS NULL)
AND ([Password] = @Password OR @Password IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
AND ([EmployeeId] = @EmployeeId OR @EmployeeId IS NULL)
AND ([EmployeeLevel] = @EmployeeLevel OR @EmployeeLevel IS NULL)
AND ([SupervisorId] = @SupervisorId OR @SupervisorId IS NULL)
AND ([EmployeeCreatedBy] = @EmployeeCreatedBy OR @EmployeeCreatedBy IS NULL)
AND ([EmployeeCreatedDate] = @EmployeeCreatedDate OR @EmployeeCreatedDate IS NULL)
AND ([EmployeeModifiedBy] = @EmployeeModifiedBy OR @EmployeeModifiedBy IS NULL)
AND ([EmployeeModifiedDate] = @EmployeeModifiedDate OR @EmployeeModifiedDate IS NULL)
