﻿CREATE TABLE [dbo].[Reclaim] (
    [ReclaimId] INT    IDENTITY(1,1)      NOT NULL,
    [VisitHeaderId] INT          NOT NULL,
    [Description]   NVARCHAR(100)   NOT NULL,
	[Active]		BIT	NOT NULL DEFAULT 1,
    [CreatedBy]     VARCHAR (50) NOT NULL,
    [CreatedDate]   DATETIME      NOT NULL,
    [ModifiedBy]    VARCHAR (50) NULL,
    [ModifiedDate]  DATETIME     NULL,
    CONSTRAINT [PK_Reclaim] PRIMARY KEY CLUSTERED ([ReclaimId] ASC),
    CONSTRAINT [FK_Reclaim_VisitHeader] FOREIGN KEY ([VisitHeaderId]) REFERENCES [dbo].[VisitHeader] ([VisitHeaderId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Reclaim]
    ON [dbo].[Reclaim]([VisitHeaderId] ASC);

