﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;

namespace IndexaWeb.Intranet.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize(Roles = "Administrador")]
    public class LocationsController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        // GET: Locations
        [AllowAnonymous]
        public async Task<ActionResult> Index(int? id, string name)
        {
            ViewBag.id = id;
            ViewBag.name = name;
            return View(await db.Location.Where(x => (x.CustomerId == id || id == null) && x.Active == true).ToListAsync());
        }

        // GET: Locations/Details/5
        public async Task<ActionResult> Details(int? id, int? customerId, string name)
        {
            ViewBag.id = id;
            ViewBag.customerId = customerId;
            ViewBag.name = name;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location locations = await db.Location.FindAsync(id);
            if (locations == null)
            {
                return HttpNotFound();
            }
            return View(locations);
        }

        // GET: Locations/Create
        public ActionResult Create(int? id, int? customerId, string name)
        {
            ViewBag.customerId = customerId;
            ViewBag.name = name;
            return View();
        }

        // POST: Locations/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Location location, int customerId, string name)
        {
            if (ModelState.IsValid)
            {
                location.CustomerId = customerId;
                location.Active = true;
                location.CreatedBy = User.Identity.Name;
                location.CreatedDate = DateTime.Now;
                db.Location.Add(location);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new { id = location.CustomerId, name = name });
            }

            return View(location);
        }

        // GET: Locations/Edit/5
        public async Task<ActionResult> Edit(int? id, int? customerId, string name)
        {
            ViewBag.id = id;
            ViewBag.name = name;
            ViewBag.customerId = customerId;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location locations = await db.Location.FindAsync(id);
            if (locations == null)
            {
                return HttpNotFound();
            }
            return View(locations);
        }

        // POST: Locations/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "LocationId,Description,CustomerId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] Location locations, int id, int customerId, string name)
        {
            ViewBag.id = id;
            ViewBag.name = name;
            ViewBag.customerId = customerId;
            if (ModelState.IsValid)
            {
                locations.ModifiedBy = User.Identity.Name;
                locations.ModifiedDate = DateTime.Now;
                db.Entry(locations).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new { id = customerId, name = name });
            }
            return View(locations);
        }

        // GET: Locations/Delete/5
        public async Task<ActionResult> Delete(int? id, int? customerId, string name)
        {
            ViewBag.name = name;
            ViewBag.customerId = customerId;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location locations = await db.Location.FindAsync(id);
            if (locations == null)
            {
                return HttpNotFound();
            }
            return View(locations);
        }

        // POST: Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int? customerId, string name)
        {
            ViewBag.name = name;
            ViewBag.customerId = customerId;
            Location locations = await db.Location.FindAsync(id);
            locations.Active = false;
            locations.ModifiedBy = User.Identity.Name;
            locations.ModifiedDate = DateTime.Now;
            db.Entry(locations).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index", new { id = customerId, name = name });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
