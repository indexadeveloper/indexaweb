﻿CREATE PROC ap_Person_Update
@PersonId int=NULL
,@Name nvarchar (50)=NULL
,@LastName nvarchar (50)=NULL
,@Phone nvarchar (50)=NULL
,@Email nvarchar (50)=NULL
,@Login nvarchar (50)=NULL
,@Password nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE Person 
SET 
[Name]=ISNULL(@Name,Name)
,[LastName]=ISNULL(@LastName,LastName)
,[Phone]=ISNULL(@Phone,Phone)
,[Email]=ISNULL(@Email,Email)
,[Login]=ISNULL(@Login,Login)
,[Password]=ISNULL(@Password,Password)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [PersonId]=@PersonId
