﻿CREATE PROC ap_ElementPermission_Insert
@Element_Id int=NULL
,@Permissions_Id int=NULL
AS
INSERT INTO ElementPermission( 
[Element_Id]
,[Permissions_Id]
)
VALUES ( 
@Element_Id
,@Permissions_Id
)
SELECT SCOPE_IDENTITY() AS Id
