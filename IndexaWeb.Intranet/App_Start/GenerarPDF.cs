﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace IndexaWeb.Intranet.App_Start
{
    public partial class GenerarPdf : System.Web.UI.Page
    {
        public void f_generarPDF(List<Recibos> datos, string nombreArch, PdfPTable Error)
        {
            try
            { 
                MemoryStream ms = new MemoryStream();
                Document document = new Document(PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(document, ms);
                document.Open();
                int cont = 0;
                foreach(var item in datos)
                {
                    cont++;
                    document.Add(item.getTabla());
                    if (cont == 2)
                    { 
                        cont = 0;
                        document.NewPage();
                    }
                }
                document.Close();
                writer.Close();
                byte[] bytes = ms.ToArray();
                ms.Close();
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + nombreArch);
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                HttpContext.Current.Response.BinaryWrite(bytes);
                HttpContext.Current.Response.End();
                HttpContext.Current.Response.Close();
            }
            catch (Exception e)
            {
                MemoryStream ms = new MemoryStream();
                Document document = new Document(PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(document, ms);
                document.Open();
                document.Add(Error);
                document.Close();
                writer.Close();
                byte[] bytes = ms.ToArray();
                ms.Close();
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Error de Rango.pdf");
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                HttpContext.Current.Response.BinaryWrite(bytes);
                HttpContext.Current.Response.End();
                HttpContext.Current.Response.Close();
            }
        }
    }
}