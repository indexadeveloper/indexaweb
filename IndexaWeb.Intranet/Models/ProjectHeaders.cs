﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IndexaWeb.Intranet.Models
{
    [Table("ProjectHeader")]
    public class ProjectHeaders
    {
        IndexaWebModel db = new IndexaWebModel();
        [Key]
        [Display(Name = "Nro")]
        public int ProjectHeaderId { get; set; }
        [Required(ErrorMessage = "El Campo Cliente es requerido")]
        [Display(Name = "Cliente")]
        public int CustomerId { get; set; }
        [Required(ErrorMessage = "El campo es obligatorio")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Display(Name = "Creado por")]
        public string CreatedBy { get; set; }
        [Display(Name = "Fecha de creacion")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Modificado por")]
        public string ModifiedBy { get; set; }
        [Display(Name = "Fecha de modificacion")]
        public DateTime? ModifiedDate { get; set; }
        public virtual Customer Customer { get; set; }

        public int Tareas_Completadas
        {
            get
            {
                return tareas_completadas(this.ProjectHeaderId);
            }
        }

        public int tareas_completadas(int id)
        {
            List<int> listStatus = new List<int>();
            int cont = 0;

            foreach (var item in db.StatusTask.ToList())
            {
                listStatus.Add(item.StatusTaskID);
            }

            foreach (var item in db.ProjectTask.Where(x => x.ProjectHeaderId == id).ToList())
            {
                    if (item.StatusTaskId == listStatus[2])
                        cont++;
            }

            return cont;
        }

        public int Maximo
        {
            get
            {
                return maximo(this.ProjectHeaderId);
            }
        }

        public int maximo(int id)
        {
            List<int> listStatus = new List<int>();
            List<int> listCont = new List<int>();
            int valor = -1;
            int j;

            foreach (var item in db.StatusTask.ToList())
            {
                listStatus.Add(item.StatusTaskID);
                listCont.Add(0);
            }

            foreach (var item in db.ProjectTask.Where(x => x.ProjectHeaderId == id).ToList())
            {
                for (int i = 0; i < listStatus.Count; i++)
                {
                    if (item.StatusTaskId == listStatus[i])
                        listCont[i]++;
                }
            }

            j = 0;
            do
            {
                if (listCont[j] > 0)
                    valor = listStatus[j];
                else if (j + 1 == listStatus.Count)
                    valor = 1;
                j++;
            } while (valor == -1);

            return valor;
        }

        public int Total_Horas
        {
            get
            {
                return total_horas(this.ProjectHeaderId);
            }
        }

        public int total_horas(int id)
        {
            int cont = 0;
            foreach (var item in db.ProjectTask.Where(x => x.ProjectHeaderId == id).ToList())
            {
                cont = cont + item.Hours;               
            }
            return cont;
        }

        public int Total_Tareas
        {
            get
            {
                return total_tareas(this.ProjectHeaderId);
            }
        }

        public int total_tareas(int id)
        {
            return db.ProjectTask.Where(x => x.ProjectHeaderId == id).ToList().Count;   
        }
    }
}