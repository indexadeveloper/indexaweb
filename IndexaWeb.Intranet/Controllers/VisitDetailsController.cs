﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;

namespace IndexaWeb.Intranet.Controllers
{
    public class VisitDetailsController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        // GET: VisitDetails
        public async Task<ActionResult> Index(int? visitHeaderId)
        {
            var visitDetail = db.VisitDetail.Include(v => v.VisitHeader).Where(x => (x.VisitHeaderId == visitHeaderId || visitHeaderId == null) && x.Active == true);
            return View(await visitDetail.ToListAsync());
        }

        public async Task<ActionResult> Index(VisitHeader visitHeader)
        {
            var visitDetail = db.VisitDetail.Where(v => v.VisitHeaderId == visitHeader.VisitHeaderId);            
            return View(await visitDetail.ToListAsync());
        }

        // GET: VisitDetails/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitDetail visitDetail = await db.VisitDetail.FindAsync(id);
            if (visitDetail == null)
            {
                return HttpNotFound();
            }
            return View(visitDetail);
        }

        // GET: VisitDetails/Create
        public ActionResult Create()
        {
            ViewBag.Activities = new SelectList(db.Activity.ToList(), "ActivityId", "Description");
            return View();
        }

        // POST: VisitDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(VisitDetail visitDetail)
        {
            if (ModelState.IsValid)
            {
                visitDetail.Active = true;
                visitDetail.CreatedBy = User.Identity.Name;
                visitDetail.CreatedDate = DateTime.Now;
                db.VisitDetail.Add(visitDetail);
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", "VisitHeader", new { id = visitDetail.VisitHeaderId });
            }
            ViewBag.ActivityId = new SelectList(db.Activity, "ActivityId", "Description", visitDetail.ActivityId);
            return View(visitDetail);
        }

        // GET: VisitDetails/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitDetail visitDetail = await db.VisitDetail.FindAsync(id);
            if (visitDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.VisitHeaderid = new SelectList(db.VisitHeader, "VisitHeaderId", "CreatedBy", visitDetail.VisitHeaderId);
            return View(visitDetail);
        }

        // POST: VisitDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "VisitDetailId,VisitHeaderId,ActivityId,VisitNotes,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] VisitDetail visitDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(visitDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.VisitHeaderid = new SelectList(db.VisitHeader, "VisitHeaderId", "CreatedBy", visitDetail.VisitHeaderId);
            return View(visitDetail);
        }

        // GET: VisitDetails/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VisitDetail visitDetail = await db.VisitDetail.FindAsync(id);
            if (visitDetail == null)
            {
                return HttpNotFound();
            }
            return View(visitDetail);
        }

        // POST: VisitDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            VisitDetail visitDetail = await db.VisitDetail.FindAsync(id);
            visitDetail.Active = false;
            visitDetail.ModifiedBy = User.Identity.Name;
            visitDetail.ModifiedDate = DateTime.Now;
            db.Entry(visitDetail).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
