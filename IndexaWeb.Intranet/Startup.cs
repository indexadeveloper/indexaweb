﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IndexaWeb.Intranet.Startup))]
namespace IndexaWeb.Intranet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
