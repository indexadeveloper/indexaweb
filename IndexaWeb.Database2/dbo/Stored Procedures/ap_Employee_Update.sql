﻿CREATE PROC ap_Employee_Update
@EmployeeId int=NULL
,@UserName int=NULL
,@SupervisorId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE Employee 
SET 
[UserName]=ISNULL(@UserName,UserName)
,[SupervisorId]=ISNULL(@SupervisorId,SupervisorId)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [EmployeeId]=@EmployeeId
