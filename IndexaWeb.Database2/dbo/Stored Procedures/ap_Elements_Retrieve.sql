﻿CREATE PROC ap_Elements_Retrieve
@Id int=NULL
,@Name nvarchar (MAX)=NULL
,@Aplications_Id nvarchar (128)=NULL
AS
SELECT 
[Id]
,[Name]
,[Aplications_Id]
FROM Elements
WHERE
 ([Id] = @Id OR @Id IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
AND ([Aplications_Id] = @Aplications_Id OR @Aplications_Id IS NULL)
