﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.Models
{
    [Table("ProjectTask")]
    public class ProjectTask : IValidatableObject
    {
        [Key]
        [Display(Name = "Nro")]
        public int ProjectTaskID { get; set; }
        [Display(Name = "ID de el proyecto")]
        public int ProjectHeaderId { get; set; }
        [Required(ErrorMessage = "El campo es obligatorio")]
        [Display(Name = "Nombre")]
        public string TaskName { get; set; }
        [Display(Name = "Fecha de publicacion")]
        public DateTime PublicationDate { get; set; }
        [Display(Name = "Fecha de inicio")]
        public DateTime? StartingDate { get; set; }
        [Display(Name = "Fecha estimada")]
        public DateTime? EstimatedEnding { get; set; }
        [Display(Name = "Fecha de entrega")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "Observaciones")]
        public string TaskObservation { get; set; }
        [Display(Name = "Estatus")]
        public int StatusTaskId { get; set; }
        [Display(Name = "Asignado a")]
        public string AssignedToUser { get; set; }
        [Display(Name = "Horas")]
        public int Hours { get; set; }

        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            
            if (StartingDate >= EstimatedEnding)
                yield return new ValidationResult("La fecha estimada no puede ser menor a la de inicio", new[] { "EstimatedEnding" });
        }
    }
}