﻿namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    
    [Table("Level")]
    public class Level
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Level()
        {
            EmployeeLevel = new HashSet<EmployeeLevel>();
        }
        [Key]
        public int LevelId { get; set; }
        [Display(Name = "Nombre del Cargo")]
        public string Name { get; set; }


        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<EmployeeLevel> EmployeeLevel { get; set; }
    }
}
