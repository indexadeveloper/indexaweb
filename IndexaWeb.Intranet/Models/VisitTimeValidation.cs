﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexaWeb.Intranet.Models
{
    public class VisitTimeValidation : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            TimeSpan visitTime = (TimeSpan)value;
            VisitHeader visitHeader = (VisitHeader)validationContext.ObjectInstance;


            //if (visitHeader.VisitDate == DateTime.Today)
            //{
            //    if (visitTime > DateTime.Now.TimeOfDay)
            //    {
            //        return new ValidationResult("La Hora de Llegada o Salida no puede ser posterior a la hora actual");
            //    }
            //}
            if (visitHeader.ArrivalTime > visitHeader.DepatureTime)
            {
                return new ValidationResult("La Hora de Salida no puede ser anterior a la Hora de Entrada");
            }
            return ValidationResult.Success;
        }
    }
}
