﻿CREATE TABLE [dbo].[ReclaimComment] (
    [ReclaimCommentId] INT            IDENTITY (1, 1) NOT NULL,
    [Description]      NVARCHAR (500) NULL,
    [ReclaimId]        INT            NULL,
	[Active]		BIT	NOT NULL DEFAULT 1,
    [CreatedBy]        VARCHAR (50)   NULL,
    [CreatedDate]      DATETIME        NULL,
    [ModifiedBy]       VARCHAR (50)   NULL,
    [ModifiedDate]     DATETIME       NULL,
    CONSTRAINT [PK_ReclaimComment] PRIMARY KEY CLUSTERED ([ReclaimCommentId] ASC),
    CONSTRAINT [FK_ReclaimComment_Reclaim] FOREIGN KEY ([ReclaimId]) REFERENCES [dbo].[Reclaim] ([ReclaimId])
);

