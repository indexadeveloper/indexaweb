﻿CREATE PROC ap_ProjectTask_Insert
@ProjectHeaderId int=NULL
,@TaskName nvarchar (100)=NULL
,@PublicationDate datetime=NULL
,@StartingDate datetime=NULL
,@EstimatedEnding datetime=NULL
,@EndDate datetime=NULL
,@TaskObservation nvarchar (500)=NULL
,@StatusTaskId int=NULL
AS
INSERT INTO ProjectTask( 
[ProjectHeaderId]
,[TaskName]
,[PublicationDate]
,[StartingDate]
,[EstimatedEnding]
,[EndDate]
,[TaskObservation]
,[StatusTaskId]
)
VALUES ( 
@ProjectHeaderId
,@TaskName
,@PublicationDate
,@StartingDate
,@EstimatedEnding
,@EndDate
,@TaskObservation
,@StatusTaskId
)
SELECT SCOPE_IDENTITY() AS Id
