﻿namespace IndexaWeb.Intranet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;

    [Table("StatusVisitHistory")]
    public class StatusVisitHistory
    {
        [Key]
        public int StatusVisitHistoryId { get; set; }
        public int StatusVisitId { get; set; }
        public int VisitHeaderId { get; set; }
        public string Comment { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}