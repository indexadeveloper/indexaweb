namespace IndexaWeb.Intranet
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Web.Mvc;

    [Table("Employee")]
    public partial class Employee
    {
        private IndexaWebModel db = new IndexaWebModel();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            CustomersPerEmployee = new HashSet<CustomersPerEmployee>();
            VisitHeader = new HashSet<VisitHeader>();
            EmployeeLevel = new HashSet<EmployeeLevel>();
        }
        [Key]
        public int EmployeeId { get; set; }
        
        [Display(Name = "Nombre de Usuario")]
        public string UserName { get; set; }
        [Display(Name = "Empleado")]
        public string EmployeeName
        {
            get
            {
                return db.AspNetUsers.Where(x => x.UserName == this.UserName).Select(x => x.Name + " " + x.LastName).FirstOrDefault();
            }
        }

        [Display(Name = "Supervisor Directo")]
        public int? SupervisorId { get; set; }

        [Display(Name = "Cedula")]
        public string PersonId { get; set; }

        public string SupervisorName
        {
            get
            {
                return (from j in db.Employee
                        join k in db.AspNetUsers
                        on j.UserName equals k.UserName
                        where j.EmployeeId == this.SupervisorId
                        select k.Name + " " + k.LastName).FirstOrDefault();
            }
        }
        
        public string Email { get { return db.AspNetUsers.Where(u => u.UserName == UserName).Select(e => e.Email).FirstOrDefault(); }  }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }
        
        [Display(Name = "Creado El")]
        public DateTime? CreatedDate { get; set; }
        
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }
        
        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }

        [Display(Name = "Cargos")]
        public IEnumerable<int> SelectedLevels { get; set; }

        public IEnumerable<SelectListItem> EmployeeLevels { get; set; }
        
        public IEnumerable<SelectListItem> Supervisors { get; set; }

        public IEnumerable<SelectListItem> UserNames { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomersPerEmployee> CustomersPerEmployee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitHeader> VisitHeader { get; set; }

        [Display(Name = "Cargo")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeLevel> EmployeeLevel { get; set; }

    }
}
