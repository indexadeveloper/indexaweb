﻿CREATE PROC ap_RoleGroup_Retrieve
@Groups_Id int=NULL
,@Roles_Id int=NULL
AS
SELECT 
[Groups_Id]
,[Roles_Id]
FROM RoleGroup
WHERE
 ([Groups_Id] = @Groups_Id OR @Groups_Id IS NULL)
AND ([Roles_Id] = @Roles_Id OR @Roles_Id IS NULL)
