﻿using System;
using System.ComponentModel.DataAnnotations;


namespace IndexaWeb.Intranet.Models
{
    public class ProjectTaskModels
    {
       [Key]
       public int ProjectTaskID { get; set; }
       public int StatusTaskID { get; set; }
       public string TaskName { get; set; }
       public DateTime PublicationDate { get; set; }
       public DateTime StartingDate { get; set; }
       public DateTime EstimatedEnding { get; set; }
       public DateTime EndDate { get; set; }
    }
}