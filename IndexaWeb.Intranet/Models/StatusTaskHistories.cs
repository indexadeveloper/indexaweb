﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.Models
{
    [Table("StatusTaskHistories")]
    public class StatusTaskHistories
    {
        [Key]
        [Display(Name = "ID")]
        public int StatusTaskHistoryID { get; set; }
        [Display(Name = "ID del estado")]
        public int StatusTaskID { get; set; }
        [Display(Name = "ID del proyecto")]
        public int ProjectHeaderID { get; set; }
        [Display(Name = "Fecha de cambio")]
        public DateTime ChangeDate { get; set; }
        [Display(Name = "ID de la tarea")]
        public int ProjectTaskID { get; set; }
    }
}