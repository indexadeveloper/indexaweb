﻿CREATE PROC ap_Groups_Retrieve
@Id int=NULL
,@Name nvarchar (MAX)=NULL
,@Status bit=NULL
,@Aplication_Id nvarchar (128)=NULL
AS
SELECT 
[Id]
,[Name]
,[Status]
,[Aplication_Id]
FROM Groups
WHERE
 ([Id] = @Id OR @Id IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
AND ([Status] = @Status OR @Status IS NULL)
AND ([Aplication_Id] = @Aplication_Id OR @Aplication_Id IS NULL)
