﻿CREATE PROC ap_Groups_Insert
@Name nvarchar (MAX)=NULL
,@Status bit=NULL
,@Aplication_Id nvarchar (128)=NULL
AS
INSERT INTO Groups( 
[Name]
,[Status]
,[Aplication_Id]
)
VALUES ( 
@Name
,@Status
,@Aplication_Id
)
SELECT SCOPE_IDENTITY() AS Id
