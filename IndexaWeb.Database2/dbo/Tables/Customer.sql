﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]   INT           IDENTITY (1, 1) NOT NULL,
    [Rif]          NVARCHAR (50) NULL,
    [Name]         NVARCHAR (50) NULL,
    [Alias]        NVARCHAR (50) NULL,
	[Active]	   BIT	NOT NULL DEFAULT 1,
    [CreatedBy]    VARCHAR (50)  NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [ModifiedBy]   VARCHAR (50)  NULL,
    [ModifiedDate] DATETIME      NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);

