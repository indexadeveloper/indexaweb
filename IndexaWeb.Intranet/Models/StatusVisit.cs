﻿namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Table("StatusVisit")]
    public class StatusVisit
    {
        public StatusVisit()
        {
            VisitHeader = new HashSet<VisitHeader>();
        }

        [Key]
        public int StatusVisitId { get; set; }
        [Display(Name = "Estado")]
        public string Name { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public ICollection<VisitHeader> VisitHeader { get; set; }
    }
}
