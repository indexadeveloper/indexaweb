﻿CREATE PROC ap_ReclaimComment_Update
@ReclaimCommentId int=NULL
,@Description nvarchar (500)=NULL
,@ReclaimId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE ReclaimComment 
SET 
[Description]=ISNULL(@Description,Description)
,[ReclaimId]=ISNULL(@ReclaimId,ReclaimId)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [ReclaimCommentId]=@ReclaimCommentId
