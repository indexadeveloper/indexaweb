﻿CREATE PROC ap_Customer_Update
@CustomerId int=NULL
,@Rif nvarchar (50)=NULL
,@Name nvarchar (50)=NULL
,@Alias nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE Customer 
SET 
[Rif]=ISNULL(@Rif,Rif)
,[Name]=ISNULL(@Name,Name)
,[Alias]=ISNULL(@Alias,Alias)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [CustomerId]=@CustomerId
