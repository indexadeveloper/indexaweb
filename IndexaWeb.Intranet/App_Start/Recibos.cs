﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.App_Start
{
    public class Recibos
    {
        private PdfPTable Re;
        private int MES;

        public Recibos()
        {
            this.Re = null;
            this.MES = 0;
        }

        public void setRecibo(PdfPTable tabla,int i)
        {
            this.Re = tabla;
            this.MES = i;
        }

        public PdfPTable getTabla()
        {
            return this.Re;
        }

        public int getMes()
        {
            return this.MES;
        }
    }
}