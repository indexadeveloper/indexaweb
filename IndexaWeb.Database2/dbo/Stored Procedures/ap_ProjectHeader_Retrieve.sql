﻿CREATE PROC ap_ProjectHeader_Retrieve
@ProjectHeaderId int=NULL
,@CustomerId int=NULL
AS
SELECT 
[ProjectHeaderId]
,[CustomerId]
FROM ProjectHeader
WHERE
 ([ProjectHeaderId] = @ProjectHeaderId OR @ProjectHeaderId IS NULL)
AND ([CustomerId] = @CustomerId OR @CustomerId IS NULL)
