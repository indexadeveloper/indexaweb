namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;
    [Table("VisitDetail")]
    public partial class VisitDetail
    {
        public IEnumerable<SelectListItem> Activities { get; set; }

        [Key]
        public int VisitDetailId { get; set; }

        public int VisitHeaderId { get; set; }

        [Display(Name = "Actividad Realizada")]
        public int ActivityId { get; set; }
        
        [StringLength(500)]
        [Display(Name = "Notas de la Visita")]
        public string VisitNotes { get; set; }


        [Display(Name = "Activo")]
        public bool Active { get; set; }
        [Display(Name = "Ticket")]
        public int? TicketNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }

        [Display(Name = "Creado El")]
        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }

        public virtual VisitHeader VisitHeader { get; set; }

        public virtual Activity Activity { get; set; }
    }
}
