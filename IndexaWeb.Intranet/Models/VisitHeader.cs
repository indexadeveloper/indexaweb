namespace IndexaWeb.Intranet
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;
    [Table("VisitHeader")]
    public partial class VisitHeader
    {

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VisitHeader()
        {
            Reclaims = new HashSet<Reclaim>();
            VisitDetail = new HashSet<VisitDetail>();
        }
        [Key]
        [Display(Name = "Visita")]
        public int VisitHeaderId { get; set; }

        [Display(Name = "Empleado")]
        public int EmployeeId { get; set; }

        [Required(ErrorMessage = "El Campo Cliente es requerido")]
        [Display(Name = "Cliente")]
        public int CustomerId { get; set; }

        [Required(ErrorMessage = "El Campo Ubicaci�n es requerido")]
        [Display(Name = "Ubicaci�n")]
        public int LocationId { get; set; }

        [Required(ErrorMessage = "El Campo Fecha Visita es requerido")]
        [Display(Name = "Fecha Visita")]
        [VisitDateValidation]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime VisitDate { get; set; }

        [Required(ErrorMessage = "El Campo Hora Llegada es requerido")]
        [Display(Name = "Hora Llegada")]
        [VisitTimeValidation]
        public TimeSpan ArrivalTime { get; set; }
        [Required(ErrorMessage = "El Campo Hora Salida es requerido")]
        [Display(Name = "Hora Salida")]
        [VisitTimeValidation]
        public TimeSpan DepatureTime { get; set; }
        [Display(Name = "Estatus")]
        public int StatusVisitId { get; set; }
        [Display(Name = "Activo")]
        public bool Active { get; set; }
        [Display(Name = "Direcci�n IP Origen")]
        public string IpAddress { get; set; }

        [StringLength(50)]
        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }
        [Display(Name = "Creado El")]
        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }
        [Display(Name = "Modificado El")]
        public DateTime? ModifiedDate { get; set; }

        public IEnumerable<SelectListItem> Locations { get; set; }
        
        public virtual Customer Customer { get; set; }
        
        public virtual Employee Employee { get; set; }
        
        public virtual Location Location { get; set; }
        
        public virtual StatusVisit StatusVisit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reclaim> Reclaims { get; set; }
                
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitDetail> VisitDetail { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    //if (this.VisitDate > DateTime.Today && (this.StatusVisitId == 1 || this.StatusVisitId == 2))
        //    //    yield return new ValidationResult("La fecha de la visita no puede ser posterior a la fecha actual", new[] { "VisitDate" });

        //    if (ArrivalTime > DateTime.Now.TimeOfDay && this.VisitDate == DateTime.Today && (this.StatusVisitId == 1 || this.StatusVisitId == 2))
        //    {
        //        yield return new ValidationResult("La Hora de Llegada no puede ser posterior a la Hora actual", new[] { "ArrivalTime" });
        //    }
        //    if (DepatureTime > DateTime.Now.TimeOfDay && this.VisitDate == DateTime.Today && (this.StatusVisitId == 1 || this.StatusVisitId == 2))
        //    {
        //        yield return new ValidationResult("La Hora de Salida no puede ser posterior a la Hora actual", new[] { "DepatureTime" });
        //    }
        //    if (ArrivalTime > DepatureTime && (this.StatusVisitId == 1 || this.StatusVisitId == 2))
        //    {
        //        yield return new ValidationResult("La Hora de Salida no puede ser anterior a la Hora de Entrada", new[] { "DepatureTime" });
        //    }

        //}
    }
}
