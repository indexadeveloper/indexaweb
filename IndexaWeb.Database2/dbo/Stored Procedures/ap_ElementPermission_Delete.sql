﻿CREATE PROC ap_ElementPermission_Delete
@Element_Id int=NULL
,@Permissions_Id int=NULL
AS
DELETE ElementPermission 
WHERE
 [Element_Id]=@Element_Id
AND [Permissions_Id]=@Permissions_Id
