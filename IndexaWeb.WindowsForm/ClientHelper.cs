﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IndexaWeb.WindowsForm
{
    public static class ClientHelper
    {
        public static HttpClient GetClient(string username, string password)
        {
            var authValue = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}")));

            var client = new HttpClient()
            {                
                DefaultRequestHeaders = { Authorization = authValue  },
                BaseAddress = new Uri($"http://localhost:65373/"),                
                Timeout = TimeSpan.FromDays(2)
            };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }
    }
}
