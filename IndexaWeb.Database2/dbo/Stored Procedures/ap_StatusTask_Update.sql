﻿CREATE PROC ap_StatusTask_Update
@StatusTaskID int=NULL
,@Name nvarchar (50)=NULL
AS
UPDATE StatusTask 
SET 
[Name]=ISNULL(@Name,Name)
WHERE
 [StatusTaskID]=@StatusTaskID
