﻿CREATE PROC ap_Representative_Update
@RepresentativeId int=NULL
,@UserName int=NULL
,@CustomerId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE Representative 
SET 
[UserName]=ISNULL(@UserName,UserName)
,[CustomerId]=ISNULL(@CustomerId,CustomerId)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [RepresentativeId]=@RepresentativeId
