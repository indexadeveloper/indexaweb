﻿CREATE PROC ap_RoleGroup_Insert
@Groups_Id int=NULL
,@Roles_Id int=NULL
AS
INSERT INTO RoleGroup( 
[Groups_Id]
,[Roles_Id]
)
VALUES ( 
@Groups_Id
,@Roles_Id
)
SELECT SCOPE_IDENTITY() AS Id
