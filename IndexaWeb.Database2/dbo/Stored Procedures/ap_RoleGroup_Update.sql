﻿CREATE PROC ap_RoleGroup_Update
@Groups_Id int=NULL
,@Roles_Id int=NULL
AS
UPDATE RoleGroup 
SET 
[Groups_Id]=ISNULL(@Groups_Id,Groups_Id)
,[Roles_Id]=ISNULL(@Roles_Id,Roles_Id)
WHERE
 [Groups_Id]=@Groups_Id
AND [Roles_Id]=@Roles_Id
