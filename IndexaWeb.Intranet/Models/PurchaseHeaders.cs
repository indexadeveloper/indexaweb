﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.Models
{
    [Table("PurchaseHeaders")]
    public class PurchaseHeaders
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Rif")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Code { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Name { get; set; }
        [Display(Name = "Factura")]
        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Invoice { get; set; }
    }
}