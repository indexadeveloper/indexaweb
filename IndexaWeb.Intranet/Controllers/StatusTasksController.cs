﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;

namespace IndexaWeb.Intranet.Controllers
{
    public class StatusTasksController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        public async Task<ActionResult> Update([Bind(Include = "StatusTaskID,Name")] StatusTask statusTasks, int idt, int id, string name, int statusId)
        {
            ProjectTasksController newTask = new ProjectTasksController();
            ProjectTask projectTask = db.ProjectTask.Where(x => x.ProjectTaskID == id).First();
            ProjectHeaders projectHeader = db.ProjectHeader.Where(x => x.ProjectHeaderId == projectTask.ProjectHeaderId).First();



            switch(statusId)
            {
                case 2:
                    if (projectTask.StatusTaskId == 1)
                    { 
                        projectTask.AssignedToUser = User.Identity.Name;
                        projectTask.StatusTaskId = statusId;
                    }
                    break;
                case 4:
                    if (projectTask.StatusTaskId == 1)
                    {
                        projectTask.EndDate = DateTime.Now;
                        projectTask.StatusTaskId = statusId;
                    }
                    break;
                case 3:
                    projectTask.StatusTaskId = statusId;
                    projectTask.EndDate = DateTime.Now;
                    break;

            }
               
            await db.SaveChangesAsync();

            return await newTask.Editar(id, projectTask.ProjectHeaderId, projectHeader.Name, projectTask, statusId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
