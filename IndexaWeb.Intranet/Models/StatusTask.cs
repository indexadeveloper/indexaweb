﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IndexaWeb.Intranet.Models
{
    [Table("StatusTask")]
    public class StatusTask
    {
        [Key]
        [Display(Name = "ID del estatus")]
        public int StatusTaskID { get; set; }
        [Required]
        [Display(Name = "Estado actual de la tarea")]
        public string Name { get; set; }
    }
}