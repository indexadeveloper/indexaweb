﻿CREATE PROC ap_Reclaim_Update
@ReclaimId int=NULL
,@Description nchar=NULL
,@VisitHeaderId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE Reclaim 
SET 
[Description]=ISNULL(@Description,Description)
,[VisitHeaderId]=ISNULL(@VisitHeaderId,VisitHeaderId)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [ReclaimId]=@ReclaimId
