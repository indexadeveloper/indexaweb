﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IndexaWeb.Intranet
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                         name: "CustomersLocations",
                         url: "Customers/{controller}/{action}/{id}",
                         defaults: new { controller = "Locations", action = "Index", id = UrlParameter.Optional }
                     );

            routes.MapRoute(
                name: "ProjectHeadersProjectTasks",
                url: "ProjectHeaders/{controller}/{action}/{id}",
                defaults: new { controller = "ProjectTasks", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "StatusTasksProjectTasks",
                url: "StatusTasks/{controller}/{action}/{id}",
                defaults: new { controller = "ProjectTasks", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ProjectTasksStatusTasks",
                url: "ProjectTasks/{controller}/{action}/{id}",
                defaults: new { controller = "StatusTasks", action = "Update", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ProjectTasksStatusTaskHistories",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "StatusTaskHistories", action = "Index", id = UrlParameter.Optional, name = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "StatusTaskHistoriesProjectTasks",
                url: "StatusTaskHistories/{controller}/{action}/{id}",
                defaults: new { controller = "ProjectTasks", action = "Index", id = UrlParameter.Optional, name = UrlParameter.Optional }
            );

            routes.MapRoute(
                 name: "Purchase",
                 url: "PurchaseHeaders/PurchaseDetails/{controller}/{action}/{id}",
                 defaults: new { controller = "PurchaseHeaders", action = "DataPurchaseId", id = UrlParameter.Optional }
             );
        }
    }
}
