namespace IndexaWeb.Intranet.ApplicationDbContextMigrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IndexaWeb.Intranet.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"ApplicationDbContextMigrations";
        }

        protected override void Seed(IndexaWeb.Intranet.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            InitializeIdentityForEF(context);
            base.Seed(context); 
        }


        public static void InitializeIdentityForEF(ApplicationDbContext db)
        {
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<IdentityUser>(db);
            var userManager = new UserManager<IdentityUser>(userStore);
            var context = new IndexaWebModel();
            List<IdentityRole> roles = new List<IdentityRole>();

            roles.Add(new IdentityRole() { Name = "Administrador" });
            roles.Add(new IdentityRole() { Name = "Cliente" });
            roles.Add(new IdentityRole() { Name = "Soporte" });
            roles.Add(new IdentityRole() { Name = "Desarrollador" });

            foreach (var item in roles)
            {
                roleManager.Create(item);
            }

            List<ApplicationUser> users = new List<ApplicationUser>();
            users.Add(new ApplicationUser()
            {
                Name = "Administrator",
                PasswordHash = new PasswordHasher().HashPassword("Pa$$w0rd"),
                Email = "indexaweb@hotmail.com",
                LastName = "Indexa",
                UserName = "Admin",
                PhoneNumber = "55555555",
                Active = true
            });
            foreach (var item in users)
            {
                userManager.Create(item);
                userManager.AddToRole(item.Id, "Administrador");
            }
            context.Employee.Add(new Employee()
            {
                UserName = "Admin",
                SupervisorId = null,
                PersonId = "69",
                CreatedBy = "Admin",
                CreatedDate = DateTime.Now               
            });
            context.SaveChanges();

            context.EmployeeLevel.Add(new EmployeeLevel()
            {
                EmployeeId = 1,
                LevelId = 1
            });
            context.EmployeeLevel.Add(new EmployeeLevel()
            {
                EmployeeId = 1,
                LevelId = 2
            });
            context.EmployeeLevel.Add(new EmployeeLevel()
            {
                EmployeeId = 1,
                LevelId = 3
            });
            context.SaveChanges();


        }
    }
}
