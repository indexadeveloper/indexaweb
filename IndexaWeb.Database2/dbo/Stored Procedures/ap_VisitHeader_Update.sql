﻿CREATE PROC ap_VisitHeader_Update
@VisitHeaderId int=NULL
,@EmployeeId int=NULL
,@CustomerId int=NULL
,@LocationId int=NULL
,@VisitDate date=NULL
,@ArrivalTime time=NULL
,@DepatureTime time=NULL
,@StatusVisitId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE VisitHeader 
SET 
[EmployeeId]=ISNULL(@EmployeeId,EmployeeId)
,[CustomerId]=ISNULL(@CustomerId,CustomerId)
,[LocationId]=ISNULL(@LocationId,LocationId)
,[VisitDate]=ISNULL(@VisitDate,VisitDate)
,[ArrivalTime]=ISNULL(@ArrivalTime,ArrivalTime)
,[DepatureTime]=ISNULL(@DepatureTime,DepatureTime)
,[StatusVisitId]=ISNULL(@StatusVisitId,StatusVisitId)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [VisitHeaderId]=@VisitHeaderId
