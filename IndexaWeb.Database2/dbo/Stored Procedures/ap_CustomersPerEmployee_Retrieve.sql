﻿CREATE PROC ap_CustomersPerEmployee_Retrieve
@CustomersPerEmployeeId int=NULL
,@EmployeeId int=NULL
,@CustomerId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[CustomersPerEmployeeId]
,[EmployeeId]
,[CustomerId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM CustomersPerEmployee
WHERE
 ([CustomersPerEmployeeId] = @CustomersPerEmployeeId OR @CustomersPerEmployeeId IS NULL)
AND ([EmployeeId] = @EmployeeId OR @EmployeeId IS NULL)
AND ([CustomerId] = @CustomerId OR @CustomerId IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
