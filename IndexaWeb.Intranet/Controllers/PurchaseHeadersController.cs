﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;

namespace IndexaWeb.Intranet.Controllers
{
    public class PurchaseHeadersController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();
        private struct PurchaseData
        {
            public PurchaseHeaders header;
            public List<Products> product; 
        }

        private struct Count
        {
            public int products;
            public int totalProducts;
            public string color;
        }


        // GET: PurchaseHeaders
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult DataPurchase()
        {
            return Json(db.PurchaseRMAs.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PurchaseDetails(int? id)
        {
            ViewBag.id = id;
            return View();
        }

        public JsonResult DataPurchaseId(int id)
        {
            PurchaseData pd = new PurchaseData();
            pd.header = db.PurchaseRMAs.Find(id);
            pd.product = db.Products.Where(x => x.ProjectHeadersId == id).ToList();
            return Json(pd, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerData(List<string> seriales)
        {
            List<string> list = new List<string>();

            foreach (var item in seriales)
            {
                if (item != null)
                    list.Add(item);
            }

            foreach(var item in list)
            {
                int id = Convert.ToInt32(item.Substring(item.LastIndexOf(" ")).Replace(" ", ""));
                Products pro =  db.Products.Where(x => x.Id == id).ToList().First();
                pro.Serial = item.Replace(item.Substring(item.LastIndexOf(" ")), "");
                db.Entry(pro).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Cuenta(int id)
        {
            int cont = 0;
            var list = db.Products.Where(x => x.ProjectHeadersId == id).Select(x => new {x.Serial});
            Count cuenta = new Count();

            foreach (var item in list)
            {
                if (item.Serial != "")
                    cont++;
            }

            cuenta.totalProducts = list.Count();
            cuenta.products = cont;

            if (list.Count() == cont)
                cuenta.color = "Blue";
            else
                cuenta.color = "Green";

            return Json(cuenta, JsonRequestBehavior.AllowGet);
        }

        // GET: PurchaseHeaders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseHeaders purchaseHeaders = db.PurchaseRMAs.Find(id);
            if (purchaseHeaders == null)
            {
                return HttpNotFound();
            }
            return View(purchaseHeaders);
        }

        // GET: PurchaseHeaders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PurchaseHeaders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Code,Name,Invoice")] PurchaseHeaders purchaseHeaders)
        {
            if (ModelState.IsValid)
            {
                db.PurchaseRMAs.Add(purchaseHeaders);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(purchaseHeaders);
        }

        // GET: PurchaseHeaders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseHeaders purchaseHeaders = db.PurchaseRMAs.Find(id);
            if (purchaseHeaders == null)
            {
                return HttpNotFound();
            }
            return View(purchaseHeaders);
        }

        // POST: PurchaseHeaders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Code,Name,Invoice")] PurchaseHeaders purchaseHeaders)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchaseHeaders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(purchaseHeaders);
        }

        // GET: PurchaseHeaders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseHeaders purchaseHeaders = db.PurchaseRMAs.Find(id);
            if (purchaseHeaders == null)
            {
                return HttpNotFound();
            }
            return View(purchaseHeaders);
        }

        // POST: PurchaseHeaders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PurchaseHeaders purchaseHeaders = db.PurchaseRMAs.Find(id);
            db.PurchaseRMAs.Remove(purchaseHeaders);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
