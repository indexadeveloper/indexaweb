﻿CREATE PROC ap_ElementPermission_Update
@Element_Id int=NULL
,@Permissions_Id int=NULL
AS
UPDATE ElementPermission 
SET 
[Element_Id]=ISNULL(@Element_Id,Element_Id)
,[Permissions_Id]=ISNULL(@Permissions_Id,Permissions_Id)
WHERE
 [Element_Id]=@Element_Id
AND [Permissions_Id]=@Permissions_Id
