﻿CREATE PROC ap_ReclaimComment_Insert
@Description nvarchar (500)=NULL
,@ReclaimId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
INSERT INTO ReclaimComment( 
[Description]
,[ReclaimId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
)
VALUES ( 
@Description
,@ReclaimId
,@CreatedBy
,@CreatedDate
,@ModifiedBy
,@ModifiedDate
)
SELECT SCOPE_IDENTITY() AS Id
