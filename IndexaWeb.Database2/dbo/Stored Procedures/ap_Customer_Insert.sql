﻿CREATE PROC ap_Customer_Insert
@Rif nvarchar (50)=NULL
,@Name nvarchar (50)=NULL
,@Alias nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
INSERT INTO Customer( 
[Rif]
,[Name]
,[Alias]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
)
VALUES ( 
@Rif
,@Name
,@Alias
,@CreatedBy
,@CreatedDate
,@ModifiedBy
,@ModifiedDate
)
SELECT SCOPE_IDENTITY() AS Id
