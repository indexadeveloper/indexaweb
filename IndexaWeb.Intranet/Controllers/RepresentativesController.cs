﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;

namespace IndexaWeb.Intranet.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class RepresentativesController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        // GET: Representatives
        [AllowAnonymous]
        public async Task<ActionResult> Index(int? customerId = null)
        {
            ViewBag.name = "";
            if (db.Customer.Any(c => c.CustomerId == customerId))
                ViewBag.name = db.Customer.Find(customerId).Name;
            var representative = db.Representative.Include(r => r.Customer);
            return View(await representative.Where(r => r.Active == true && (r.CustomerId == customerId || customerId == null)).ToListAsync());
        }

        // GET: Representatives/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Representative representative = await db.Representative.FindAsync(id);
            if (representative == null)
            {
                return HttpNotFound();
            }
            return View(representative);
        }

        // GET: Representatives/Create
        public ActionResult Create(string userName = "")
        {
            if (!db.AspNetUsers.Any(x => x.UserName == userName))
                return RedirectToAction("Index");
            Representative representative = new Representative
            {                    
                UserName = userName
            };
            
            //ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name");
            return View(representative);
        }

        // POST: Representatives/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "RepresentativeId,UserName,CustomerId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] Representative representativeModel)
        {
            if (ModelState.IsValid)
            {
                representativeModel.Active = true;
                representativeModel.CreatedBy = User.Identity.Name;
                representativeModel.CreatedDate = DateTime.Now;
                try
                {
                    db.Representative.Add(representativeModel);
                    await db.SaveChangesAsync();
                }
                catch (Exception)
                {

                    throw;
                }
                return RedirectToAction("Index");
            }

            ViewBag.UserName = new SelectList(db.AspNetUsers, "UserName", "UserName");
            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name", representativeModel.CustomerId);
            return View(representativeModel);
        }

        // GET: Representatives/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Representative representative = await db.Representative.FindAsync(id);
            if (representative == null)
            {
                return HttpNotFound();
            }
            representative.UserNames = new SelectList(db.AspNetUsers, "UserName", "UserName", representative.UserName);
            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name", representative.CustomerId);
            return View(representative);
        }

        // POST: Representatives/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, Representative representative)
        {
            if (ModelState.IsValid)
            {
                
                Representative representativeToUpdate = db.Representative.Find(id);
                representativeToUpdate.CustomerId = representative.CustomerId;
                representativeToUpdate.ModifiedBy = User.Identity.Name;
                representativeToUpdate.ModifiedDate = DateTime.Now;
                db.Entry(representativeToUpdate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Rif", representative.CustomerId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", representative.UserName);
            return View(representative);
        }

        // GET: Representatives/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Representative representative = await db.Representative.FindAsync(id);
            if (representative == null)
            {
                return HttpNotFound();
            }
            return View(representative);
        }

        // POST: Representatives/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Representative representative = await db.Representative.FindAsync(id);
            representative.Active = false;
            representative.ModifiedBy = User.Identity.Name;
            representative.ModifiedDate = DateTime.Now;
            db.Entry(representative).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
