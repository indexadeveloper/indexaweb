﻿CREATE PROC ap_ElementRole_Insert
@Element_Id int=NULL
,@Roles_Id int=NULL
AS
INSERT INTO ElementRole( 
[Element_Id]
,[Roles_Id]
)
VALUES ( 
@Element_Id
,@Roles_Id
)
SELECT SCOPE_IDENTITY() AS Id
