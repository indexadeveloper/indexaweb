﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexaWeb.Intranet.Models
{
    public class VisitDateValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            DateTime todayDate = Convert.ToDateTime(value);
            DateTime threeDaysBefore = DateTime.Today.AddDays(-3);
            for (int i = 1; i <= 3; i++)
            {
                if (DateTime.Today.AddDays(i * -1).DayOfWeek == DayOfWeek.Sunday || DateTime.Today.AddDays(i * -1).DayOfWeek == DayOfWeek.Saturday)
                    threeDaysBefore = threeDaysBefore.AddDays(-1);

            }
            

            //switch (threeDaysBefore.DayOfWeek)
            //{
            //    case DayOfWeek.Saturday:
            //        threeDaysBefore = threeDaysBefore.AddDays(-1);
            //        break;
            //    case DayOfWeek.Sunday:
            //        threeDaysBefore = threeDaysBefore.AddDays(-2);
            //        break;
            //    case DayOfWeek.Friday:
            //        threeDaysBefore = threeDaysBefore.AddDays(-3);
            //        break;
            //    case DayOfWeek.Thursday:
            //        threeDaysBefore = threeDaysBefore.AddDays(-2);
            //        break;
            //    case DayOfWeek.Wednesday:
            //        threeDaysBefore = threeDaysBefore.AddDays(-1);
            //        break;
            //    default:
            //        break;
            //}
            var validDate = todayDate <= DateTime.Today && todayDate >= threeDaysBefore;
            VisitHeader visitHeader = (VisitHeader)context.ObjectInstance;

            if (visitHeader.StatusVisitId ==  1 && visitHeader.Active)
            {
                if (!validDate)
                {
                    return new ValidationResult("La fecha de la visita no puede ser posterior a la fecha actual y no tener mas de 3 días de antigüedad");
                }
            }
            return ValidationResult.Success;
        }
    }
}
