﻿CREATE PROC ap_StatusTask_Retrieve
@StatusTaskID int=NULL
,@Name nvarchar (50)=NULL
AS
SELECT 
[StatusTaskID]
,[Name]
FROM StatusTask
WHERE
 ([StatusTaskID] = @StatusTaskID OR @StatusTaskID IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
