﻿CREATE PROC ap_Reclaim_Retrieve
@ReclaimId int=NULL
,@Description nchar=NULL
,@VisitHeaderId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[ReclaimId]
,[Description]
,[VisitHeaderId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM Reclaim
WHERE
 ([ReclaimId] = @ReclaimId OR @ReclaimId IS NULL)
AND ([Description] = @Description OR @Description IS NULL)
AND ([VisitHeaderId] = @VisitHeaderId OR @VisitHeaderId IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
