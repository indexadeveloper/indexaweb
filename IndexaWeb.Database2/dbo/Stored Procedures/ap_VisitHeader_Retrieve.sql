﻿CREATE PROC ap_VisitHeader_Retrieve
@VisitHeaderId int=NULL
,@EmployeeId int=NULL
,@CustomerId int=NULL
,@LocationId int=NULL
,@VisitDate date=NULL
,@ArrivalTime time=NULL
,@DepatureTime time=NULL
,@StatusVisitId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[VisitHeaderId]
,[EmployeeId]
,[CustomerId]
,[LocationId]
,[VisitDate]
,[ArrivalTime]
,[DepatureTime]
,[StatusVisitId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM VisitHeader
WHERE
 ([VisitHeaderId] = @VisitHeaderId OR @VisitHeaderId IS NULL)
AND ([EmployeeId] = @EmployeeId OR @EmployeeId IS NULL)
AND ([CustomerId] = @CustomerId OR @CustomerId IS NULL)
AND ([LocationId] = @LocationId OR @LocationId IS NULL)
AND ([VisitDate] = @VisitDate OR @VisitDate IS NULL)
AND ([ArrivalTime] = @ArrivalTime OR @ArrivalTime IS NULL)
AND ([DepatureTime] = @DepatureTime OR @DepatureTime IS NULL)
AND ([StatusVisitId] = @StatusVisitId OR @StatusVisitId IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
