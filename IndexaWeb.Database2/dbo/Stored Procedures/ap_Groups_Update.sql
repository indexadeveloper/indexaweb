﻿CREATE PROC ap_Groups_Update
@Id int=NULL
,@Name nvarchar (MAX)=NULL
,@Status bit=NULL
,@Aplication_Id nvarchar (128)=NULL
AS
UPDATE Groups 
SET 
[Name]=ISNULL(@Name,Name)
,[Status]=ISNULL(@Status,Status)
,[Aplication_Id]=ISNULL(@Aplication_Id,Aplication_Id)
WHERE
 [Id]=@Id
