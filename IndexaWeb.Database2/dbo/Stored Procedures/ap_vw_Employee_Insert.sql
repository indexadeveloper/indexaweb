﻿CREATE PROC ap_vw_Employee_Insert
@PersonId int=NULL
,@Name nvarchar (50)=NULL
,@LastName nvarchar (50)=NULL
,@Phone nvarchar (50)=NULL
,@Email nvarchar (50)=NULL
,@Login nvarchar (50)=NULL
,@Password nvarchar (50)=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
,@EmployeeId int=NULL
,@EmployeeLevel int=NULL
,@SupervisorId int=NULL
,@EmployeeCreatedBy varchar (50)=NULL
,@EmployeeCreatedDate datetime=NULL
,@EmployeeModifiedBy varchar (50)=NULL
,@EmployeeModifiedDate datetime=NULL
AS
INSERT INTO vw_Employee( 
[PersonId]
,[Name]
,[LastName]
,[Phone]
,[Email]
,[Login]
,[Password]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
,[EmployeeId]
,[EmployeeLevel]
,[SupervisorId]
,[EmployeeCreatedBy]
,[EmployeeCreatedDate]
,[EmployeeModifiedBy]
,[EmployeeModifiedDate]
)
VALUES ( 
@PersonId
,@Name
,@LastName
,@Phone
,@Email
,@Login
,@Password
,@CreatedBy
,@CreatedDate
,@ModifiedBy
,@ModifiedDate
,@EmployeeId
,@EmployeeLevel
,@SupervisorId
,@EmployeeCreatedBy
,@EmployeeCreatedDate
,@EmployeeModifiedBy
,@EmployeeModifiedDate
)
SELECT SCOPE_IDENTITY() AS Id
