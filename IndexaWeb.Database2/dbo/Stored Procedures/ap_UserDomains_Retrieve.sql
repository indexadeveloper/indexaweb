﻿CREATE PROC ap_UserDomains_Retrieve
@Id int=NULL
,@Name nvarchar (MAX)=NULL
AS
SELECT 
[Id]
,[Name]
FROM UserDomains
WHERE
 ([Id] = @Id OR @Id IS NULL)
AND ([Name] = @Name OR @Name IS NULL)
