﻿CREATE PROC ap_CustomersPerEmployee_Update
@CustomersPerEmployeeId int=NULL
,@EmployeeId int=NULL
,@CustomerId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE CustomersPerEmployee 
SET 
[EmployeeId]=ISNULL(@EmployeeId,EmployeeId)
,[CustomerId]=ISNULL(@CustomerId,CustomerId)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [CustomersPerEmployeeId]=@CustomersPerEmployeeId
