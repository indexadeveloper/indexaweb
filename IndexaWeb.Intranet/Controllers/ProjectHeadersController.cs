﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IndexaWeb.Intranet;
using IndexaWeb.Intranet.Models;

namespace IndexaWeb.Intranet.Controllers
{
    public class ProjectHeadersController : Controller
    {
        private IndexaWebModel db = new IndexaWebModel();

        // GET: ProjectHeaders
        public async Task<ActionResult> Index()
        {

            try
            {
                var proyecto = db.ProjectHeader.Include(p => p.Customer);
                return View(await proyecto.ToListAsync());
            }catch(Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        // GET: ProjectHeaders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectHeaders projectHeaders = await db.ProjectHeader.FindAsync(id);
            if (projectHeaders == null)
            {
                return HttpNotFound();
            }
            return View(projectHeaders);
        }

        // GET: ProjectHeaders/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name"); ;
            return View();
        }

        // POST: ProjectHeaders/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProjectHeaders projectHeaders)
        {
            try
            {
                var value = ViewBag.value;
                projectHeaders.CreatedBy = User.Identity.Name;
                projectHeaders.CreatedDate = DateTime.Now;
                projectHeaders.ModifiedBy = User.Identity.Name;
                projectHeaders.ModifiedDate = DateTime.Now;
                if (ModelState.IsValid)
                {
                    db.ProjectHeader.Add(projectHeaders);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }

           }catch(Exception ex)
           {
              return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
           }

            ViewBag.CustomerId = new SelectList(db.Customer, "CustomerId", "Name", projectHeaders.CustomerId);
            return View(projectHeaders);
        }

        // GET: ProjectHeaders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectHeaders projectHeaders = await db.ProjectHeader.FindAsync(id);
            if (projectHeaders == null)
            {
                return HttpNotFound();
            }
            return View(projectHeaders);
        }

        // POST: ProjectHeaders/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProjectHeaderId,CustomerId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate")] ProjectHeaders projectHeaders)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var projectHeader = db.ProjectHeader.Find(projectHeaders.ProjectHeaderId);
                    projectHeader.Name = projectHeaders.Name;
                    projectHeader.ModifiedBy = User.Identity.Name;
                    projectHeader.ModifiedDate = DateTime.Now;
                    db.Entry(projectHeader).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
            return View(projectHeaders);
        }

        // GET: ProjectHeaders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectHeaders projectHeaders = await db.ProjectHeader.FindAsync(id);
            if (projectHeaders == null)
            {
                return HttpNotFound();
            }
            return View(projectHeaders);
        }

        // POST: ProjectHeaders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProjectHeaders projectHeaders = await db.ProjectHeader.FindAsync(id);
            db.ProjectHeader.Remove(projectHeaders);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
