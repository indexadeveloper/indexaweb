﻿CREATE TABLE [dbo].[Representative] (
    [RepresentativeId] INT          IDENTITY (1, 1) NOT NULL,
    [UserName]		   NVARCHAR(128)          NOT NULL,
    [CustomerId]       INT          NULL,
	[Active]		BIT	NOT NULL DEFAULT 1,
    [CreatedBy]        VARCHAR (50) NOT NULL,
    [CreatedDate]      DATETIME      NOT NULL,
    [ModifiedBy]       VARCHAR (50) NULL,
    [ModifiedDate]     DATETIME     NULL,
    CONSTRAINT [PK_Representative] PRIMARY KEY CLUSTERED ([RepresentativeId] ASC),
    CONSTRAINT [FK_Representative_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId])
);

