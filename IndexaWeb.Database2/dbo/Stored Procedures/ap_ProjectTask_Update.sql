﻿CREATE PROC ap_ProjectTask_Update
@ProjectHeaderId int=NULL
,@ProjectTaskId int=NULL
,@TaskName nvarchar (100)=NULL
,@PublicationDate datetime=NULL
,@StartingDate datetime=NULL
,@EstimatedEnding datetime=NULL
,@EndDate datetime=NULL
,@TaskObservation nvarchar (500)=NULL
,@StatusTaskId int=NULL
AS
UPDATE ProjectTask 
SET 
[ProjectHeaderId]=ISNULL(@ProjectHeaderId,ProjectHeaderId)
,[TaskName]=ISNULL(@TaskName,TaskName)
,[PublicationDate]=ISNULL(@PublicationDate,PublicationDate)
,[StartingDate]=ISNULL(@StartingDate,StartingDate)
,[EstimatedEnding]=ISNULL(@EstimatedEnding,EstimatedEnding)
,[EndDate]=ISNULL(@EndDate,EndDate)
,[TaskObservation]=ISNULL(@TaskObservation,TaskObservation)
,[StatusTaskId]=ISNULL(@StatusTaskId,StatusTaskId)
WHERE
 [ProjectTaskId]=@ProjectTaskId
