﻿CREATE PROC ap_VisitHeader_Insert
@EmployeeId int=NULL
,@CustomerId int=NULL
,@LocationId int=NULL
,@VisitDate date=NULL
,@ArrivalTime time=NULL
,@DepatureTime time=NULL
,@StatusVisitId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
INSERT INTO VisitHeader( 
[EmployeeId]
,[CustomerId]
,[LocationId]
,[VisitDate]
,[ArrivalTime]
,[DepatureTime]
,[StatusVisitId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
)
VALUES ( 
@EmployeeId
,@CustomerId
,@LocationId
,@VisitDate
,@ArrivalTime
,@DepatureTime
,@StatusVisitId
,@CreatedBy
,@CreatedDate
,@ModifiedBy
,@ModifiedDate
)
SELECT SCOPE_IDENTITY() AS Id
