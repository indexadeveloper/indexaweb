﻿CREATE PROC ap_Location_Update
@LocationId int=NULL
,@Description nvarchar (50)=NULL
,@CustomerId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
UPDATE Location 
SET 
[Description]=ISNULL(@Description,Description)
,[CustomerId]=ISNULL(@CustomerId,CustomerId)
,[CreatedBy]=ISNULL(@CreatedBy,CreatedBy)
,[CreatedDate]=ISNULL(@CreatedDate,CreatedDate)
,[ModifiedBy]=ISNULL(@ModifiedBy,ModifiedBy)
,[ModifiedDate]=ISNULL(@ModifiedDate,ModifiedDate)
WHERE
 [LocationId]=@LocationId
