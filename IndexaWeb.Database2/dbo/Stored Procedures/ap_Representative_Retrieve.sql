﻿CREATE PROC ap_Representative_Retrieve
@RepresentativeId int=NULL
,@UserName int=NULL
,@CustomerId int=NULL
,@CreatedBy varchar (50)=NULL
,@CreatedDate datetime=NULL
,@ModifiedBy varchar (50)=NULL
,@ModifiedDate datetime=NULL
AS
SELECT 
[RepresentativeId]
,[UserName]
,[CustomerId]
,[CreatedBy]
,[CreatedDate]
,[ModifiedBy]
,[ModifiedDate]
FROM Representative
WHERE
 ([RepresentativeId] = @RepresentativeId OR @RepresentativeId IS NULL)
AND ([UserName] = @UserName OR @UserName IS NULL)
AND ([CustomerId] = @CustomerId OR @CustomerId IS NULL)
AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
AND ([ModifiedDate] = @ModifiedDate OR @ModifiedDate IS NULL)
