namespace IndexaWeb.Intranet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReclaimComment")]
    public partial class ReclaimComment
    {
        public int ReclaimCommentId { get; set; }

        [StringLength(500)]
        [Display(Name = "Comentario")]
        public string Description { get; set; }

        public int? ReclaimId { get; set; }

        [StringLength(50)]
        [Display(Name = "Usuario")]
        public string CreatedBy { get; set; }
        
        [Display(Name = "Creado El")]
        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public virtual Reclaim Reclaim { get; set; }
    }
}
