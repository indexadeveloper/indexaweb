﻿CREATE TABLE [dbo].[VisitHeader] (
    [VisitHeaderId]   INT          IDENTITY (1, 1) NOT NULL,
    [EmployeeId]      INT          NOT NULL,
    [CustomerId]      INT          NOT NULL,
    [LocationId]      INT          NOT NULL,
    [VisitDate]       DATE         NULL,
    [ArrivalTime]     TIME (7)     NULL,
    [DepatureTime]    TIME (7)     NULL,
    [StatusVisitId]        INT          NOT NULL,
	[Active]		BIT	NOT NULL DEFAULT 1,
    [CreatedBy]       VARCHAR (50) NOT NULL,
    [CreatedDate]     DATETIME      NOT NULL,
    [ModifiedBy]      VARCHAR (50) NULL,
    [ModifiedDate]    DATETIME     NULL,
    [IpAddress] NVARCHAR(15) NOT NULL, 
    CONSTRAINT [PK_VisitHeader] PRIMARY KEY CLUSTERED ([VisitHeaderId] ASC),
    CONSTRAINT [FK_VisitHeader_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_VisitHeader_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitHeader_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]), 
    CONSTRAINT [FK_VisitHeader_StatusVisit] FOREIGN KEY ([StatusVisitId]) REFERENCES [StatusVisit]([StatusVisitId])
);

