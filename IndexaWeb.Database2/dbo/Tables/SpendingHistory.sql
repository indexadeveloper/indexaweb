﻿CREATE TABLE [dbo].[SpendingHistory]
(
	[SpendingHistoryId] INT NOT NULL IDENTITY PRIMARY KEY, 
    [Reference] NVARCHAR(50) NULL, 
    [Cost] DECIMAL (18,2) NULL, 
    [ProjectHeaderId] INT NULL, 
)
